//
//  AICoreData.h
//  CoreData
//
//  Created by Alexy Ibrahim on 8/24/15.
//  Copyright (c) 2015 Alexy Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface AICoreData : NSObject
+ (void)ai_insertRow:(NSDictionary *)row
            inEntity:(NSString *)entityName
managedObjectContext:(NSManagedObjectContext *)managedObjectContext
             success:(void (^)(NSManagedObject * managedObject))success
             failure:(void (^)(NSError *error))failure;

+ (void)ai_fetchRecordsForEntity:(NSString *)entityName
                        where:(NSDictionary *)whereVariables
               whereCondition:(NSString *)condition
              sortingVariable:(NSString *)sortingVariable
         managedObjectContext:(NSManagedObjectContext *)managedObjectContext
                      success:(void (^)(NSArray *fetchResult))success
                      failure:(void (^)(NSError *error))failure;

+ (void)ai_removeRecordsFromEntity:(NSString *)entityName
                          where:(NSDictionary *)whereVariables
                 whereCondition:(NSString *)condition
           managedObjectContext:(NSManagedObjectContext *)managedObjectContext
                        success:(void (^)())success
                        failure:(void (^)(NSError *error))failure;

+ (void)ai_removeRecord:(NSManagedObject *)managedObject
   managedObjectContext:(NSManagedObjectContext *)managedObjectContext
                success:(void (^)())success
                failure:(void (^)(NSError *error))failure;

+ (void)ai_saveManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
                            success:(void (^)())success
                            failure:(void (^)(NSError *error))failure;
@end
