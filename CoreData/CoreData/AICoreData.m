//
//  AICoreData.m
//  CoreData
//
//  Created by Alexy Ibrahim on 8/24/15.
//  Copyright (c) 2015 Alexy Ibrahim. All rights reserved.
//

#import "AICoreData.h"

@implementation AICoreData

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

+ (void)ai_insertRow:(NSDictionary *)row
            inEntity:(NSString *)entityName
managedObjectContext:(NSManagedObjectContext *)managedObjectContext
             success:(void (^)(NSManagedObject * managedObject))success
             failure:(void (^)(NSError *error))failure {
    
    NSManagedObject* managedObject = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:managedObjectContext];
    
    [row enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        @try {
            [managedObject setValue:obj forKey:key];
        }
        @catch (NSException *exception) {
            [NSException raise:@"Invalid Key Provided" format:@"%@", [NSString stringWithFormat:@"key %@ is not available", key]];
        }
        @finally {
        }
    }];
    
    // --------------------Execute fetch-------------------- Nov 7, 2014 - 03:44 PM
    NSError *error;
    if (![managedObjectContext save:&error]) {
        // This is a serious error saying the record could not be saved.
        // Advise the user to restart the application
        if (failure) {
            failure(error);
        }
    } else {
        if (success) {
            success(managedObject);
        }
    }
}

+ (void)ai_fetchRecordsForEntity:(NSString *)entityName
                        where:(NSDictionary *)whereVariables
               whereCondition:(NSString *)condition
              sortingVariable:(NSString *)sortingVariable
         managedObjectContext:(NSManagedObjectContext *)managedObjectContext
                      success:(void (^)(NSArray *fetchResult))success
                      failure:(void (^)(NSError *error))failure {
    
    // Define our table/entity to use
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:managedObjectContext];
    
    // Setup the fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    // --------------------Sorting-------------------- Nov 7, 2014 - 03:43 PM
    if ([sortingVariable isEqualToString:@""] || [sortingVariable isKindOfClass:[NSNull class]] || (sortingVariable == nil)) {
        // MARK: - sortingVariable is not available
    } else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortingVariable ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        [request setSortDescriptors:sortDescriptors];
    }
    
    // --------------------Sorting-------------------- Nov 7, 2014 - 03:44 PM
    if ([whereVariables isKindOfClass:[NSNull class]] || (whereVariables == nil)) {
        // MARK: - fetchRequest is not available
    } else {
        __block int i = 0;
        __block NSString *predicateString = [NSString new];
        [whereVariables enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            if (i > 0) {
                predicateString = [predicateString stringByAppendingString:[NSString stringWithFormat:@" %@ ", (([condition isEqual:@""] || [condition isKindOfClass:[NSNull class]] || (condition == nil)) ? @"OR":(([condition isEqual:@"OR"] || [condition isEqual:@"AND"]) ? condition:@"OR"))]];
            }
            i++;
            
            NSString *keyString = [NSString stringWithFormat:@"%@", key];
            NSString *objectString = [NSString stringWithFormat:@"%@", obj];
            
            predicateString = [predicateString stringByAppendingString:[NSString stringWithFormat:@"%@ = '%@'", keyString, objectString]];
            request.predicate = [NSPredicate predicateWithFormat:predicateString];
        }];
        //        NSLog(@"predicateString: %@", predicateString);
    }
    
    // --------------------Execute fetch-------------------- Nov 7, 2014 - 03:44 PM
    // Fetch the records and handle an error
    NSError *error;
    NSArray *fetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    
    if (!fetchResults || error) {
        // Handle the error.
        // This is a serious error and should advise the user to restart the application
        if (failure) {
            failure(error);
        }
    } else {
        if (success) {
            success(fetchResults);
        }
    }
}

+ (void)ai_removeRecordsFromEntity:(NSString *)entityName
                          where:(NSDictionary *)whereVariables
                 whereCondition:(NSString *)condition
           managedObjectContext:(NSManagedObjectContext *)managedObjectContext
                        success:(void (^)())success
                        failure:(void (^)(NSError *error))failure {
    
    [AICoreData ai_fetchRecordsForEntity:entityName where:whereVariables whereCondition:condition sortingVariable:nil managedObjectContext:managedObjectContext success:^(NSArray *fetchResult) {
        for (NSManagedObject * record in fetchResult) {
            [managedObjectContext deleteObject:record];
        }
        
        NSError *error = nil;
        if (![managedObjectContext save:&error]) {
            // This is a serious error saying the record could not be saved.
            // Advise the user to restart the application
            if (failure) {
                failure(error);
            }
        } else {
            if (success) {
                success();
            }
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void)ai_removeRecord:(NSManagedObject *)managedObject
   managedObjectContext:(NSManagedObjectContext *)managedObjectContext
                success:(void (^)())success
                failure:(void (^)(NSError *error))failure {
    
    [managedObjectContext deleteObject:managedObject];
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        // This is a serious error saying the record could not be saved.
        // Advise the user to restart the application
        if (failure) {
            failure(error);
        }
    } else {
        if (success) {
            success();
        }
    }
}

+ (void)ai_saveManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
                            success:(void (^)())success
                            failure:(void (^)(NSError *error))failure {
    
    NSError *error;
    if (![managedObjectContext save:&error]) {
        // This is a serious error saying the record could not be saved.
        // Advise the user to restart the application
        if (failure) {
            failure(error);
        }
    } else {
        if (success) {
            success();
        }
    }
}

@end
