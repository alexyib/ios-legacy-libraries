/*
 Copyright 2009-2013 Urban Airship Inc. All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 
 2. Redistributions in binaryform must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided withthe distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE URBAN AIRSHIP INC ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL URBAN AIRSHIP INC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import <UIKit/UIKit.h>

typedef enum _YALogLevel {
    YALogLevelUndefined = -1,
    YALogLevelNone = 0,
    YALogLevelError = 1,
    YALogLevelWarn = 2,
    YALogLevelInfo = 3,
    YALogLevelDebug = 4,
    YALogLevelTrace = 5
} YALogLevel;


#define YA_LEVEL_LOG_THREAD(level, levelString, fmt, ...) \
    do { \
    NSString *thread = ([[NSThread currentThread] isMainThread]) ? @"M" : @"B"; \
    NSLog((@"[%@] [%@] => %s [Line %d] " fmt), levelString, thread, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__); \
    } while(0)

#define YA_LEVEL_LOG_NO_THREAD(level, levelString, fmt, ...) \
    do { \
    NSLog((@"[%@] %s [Line %d] " fmt), levelString, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__); \
    } while(0)

//only log thread if #UA_LOG_THREAD is defined
#ifdef YA_LOG_THREAD
#define YA_LEVEL_LOG YA_LEVEL_LOG_THREAD
#else
#define YA_LEVEL_LOG YA_LEVEL_LOG_NO_THREAD
#endif

#define YA_LTRACE(fmt, ...) YA_LEVEL_LOG(YALogLevelTrace, @"T", fmt, ##__VA_ARGS__)
#define YA_LDEBUG(fmt, ...) YA_LEVEL_LOG(YALogLevelDebug, @"D", fmt, ##__VA_ARGS__)
#define YA_LINFO(fmt, ...) YA_LEVEL_LOG(YALogLevelInfo, @"I", fmt, ##__VA_ARGS__)
#define YA_LWARN(fmt, ...) YA_LEVEL_LOG(YALogLevelWarn, @"W", fmt, ##__VA_ARGS__)
#define YA_LERR(fmt, ...) YA_LEVEL_LOG(YALogLevelError, @"E", fmt, ##__VA_ARGS__)

#define YALOG YA_LDEBUG
#define YALog YALOG

// constants
#define kAirshipProductionServer @"https://device-api.urbanairship.com"
#define kAnalyticsProductionServer @"https://combine.urbanairship.com";

#ifdef _YA_VERSION
#define YA_VERSION @ _YA_VERSION
#else
#define YA_VERSION @ "1.1.2"
#endif

#define YA_VERSION_INTERFACE(CLASSNAME) \
@interface CLASSNAME : NSObject         \
+ (NSString *)get;                      \
@end


#define YA_VERSION_IMPLEMENTATION(CLASSNAME, VERSION_STR)   \
@implementation CLASSNAME                                   \
+ (NSString *)get {                                         \
return VERSION_STR;                                         \
}                                                           \
@end


#define SINGLETON_INTERFACE(CLASSNAME)                                                      \
+ (CLASSNAME*)shared;                                                                       \

#define SINGLETON_IMPLEMENTATION(CLASSNAME)                                                 \
\
static CLASSNAME* g_shared##CLASSNAME = nil;                                                \
\
+ (CLASSNAME*)shared                                                                        \
{                                                                                           \
static dispatch_once_t sharedOncePredicate##CLASSNAME;                                                 \
\
dispatch_once(&sharedOncePredicate##CLASSNAME, ^{                                                      \
g_shared##CLASSNAME = [[self alloc] init];                                                  \
});                                                                                         \
return g_shared##CLASSNAME;                                                                 \
}                                                                                           \
\
+ (id)allocWithZone:(NSZone*)zone                                                           \
{                                                                                           \
static dispatch_once_t allocOncePredicate##CLASSNAME;                                                  \
dispatch_once(&allocOncePredicate##CLASSNAME, ^{                                                       \
if (g_shared##CLASSNAME == nil) {                                                           \
g_shared##CLASSNAME = [super allocWithZone:zone];                                           \
}                                                                                           \
});                                                                                         \
return g_shared##CLASSNAME;                                                                 \
}                                                                                           \
\
- (id)copyWithZone:(NSZone*)zone                                                            \
{                                                                                           \
return self;                                                                                \
}                                                                                           \
\
- (id)retain                                                                                \
{                                                                                           \
return self;                                                                                \
}                                                                                           \
\
- (oneway void)release                                                                      \
{                                                                                           \
}                                                                                           \
\
- (id)autorelease                                                                           \
{                                                                                           \
return self;                                                                                \
}


#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_4_1
#define IF_IOS4_1_OR_GREATER(...) \
if (kCFCoreFoundationVersionNumber >= kCFCoreFoundationVersionNumber_iOS_4_1) \
{ \
__VA_ARGS__ \
}
#else
#define IF_IOS4_1_OR_GREATER(...)
#endif

