//
//  PickerView.h
//  PickerView
//
//  Created by Apple on 4/1/14.
//  Copyright (c) 2014 ITECH. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PickerViewDelegate <NSObject>
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
- (void)done_ButtonPressed:(UIButton *)sender;
@end


@interface PickerView : UIView
@property (nonatomic, strong) IBOutlet UIPickerView *pickerView;
@property (nonatomic, retain) NSArray *pickerData;

@property (strong, nonatomic) id <PickerViewDelegate> delegate;
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
@end
