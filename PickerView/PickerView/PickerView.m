//
//  PickerView.m
//  PickerView
//
//  Created by Apple on 4/1/14.
//  Copyright (c) 2014 ITECH. All rights reserved.
//

#import "PickerView.h"

@implementation PickerView
@synthesize pickerData = _pickerData;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    
}

- (void)setPickerData:(NSArray *)pickerData {
    if (_pickerData != pickerData) {
        _pickerData = pickerData;
    }
}

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return [self.pickerData count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    return [self.pickerData objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if ([self.delegate respondsToSelector:@selector(pickerView:didSelectRow:inComponent:)]) {
        [self.delegate pickerView:pickerView didSelectRow:row inComponent:component];
    }
}

- (IBAction)done_ButtonPressed:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(done_ButtonPressed:)]) {
        [self.delegate done_ButtonPressed:sender];
    }
}

@end
