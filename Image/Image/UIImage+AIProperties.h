//
//  UIImage+AIProperties.h
//  Test
//
//  Created by Alexy Ibrahim on 8/24/15.
//  Copyright (c) 2015 Alexy Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (AIProperties)
- (UIImage *)ai_imageTintedWithColor:(UIColor *)color withBlendMode:(CGBlendMode)blendMode destinationBlendMode:(CGBlendMode)destinationBlendMode;
- (UIImage *)ai_filledImagewithColor:(UIColor *)color;
- (UIImage *)ai_convertImageToGrayScale;
- (UIImage *)ai_negativeImage;
- (UIImage *)ai_changeWhiteColorToTransparent;
@end
