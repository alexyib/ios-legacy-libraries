//
//  Parse+AIHelper.m
//  Test
//
//  Created by Alexy Ibrahim on 8/24/15.
//  Copyright (c) 2015 Alexy Ibrahim. All rights reserved.
//

#import "Parse+AIHelper.h"
#import "AFNetworking.h"

#pragma mark - My Application
#define X_Parse_Application_Id @"jPKq1YQm1rwPqKITM8CUhquPHBpKt0uXHgxFTL7k"
#define X_Parse_Client_Key @"pVNe6M2AdTJkCZBQ6sx8dGcCfiaNXbZPyd4IkD8J"

#define ClassPath @"https://api.parse.com/1/classes"
#define LocalClassPath @"https://api.parse.com/1"

@implementation Parse (AIHelper)

+ (void)load {
    [super load];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self ai_checkApplicationPermissions:^(NSDictionary *fetchResults, NSError *error) {
            if (!error) {
                if ([fetchResults[@"results"] count] > 0) {
                    // MARK: - Ignore interactions
                    if (fetchResults[@"results"][0][@"ignoreInteractions"]) {
                        BOOL ignoreInteractions = [fetchResults[@"results"][0][@"ignoreInteractions"] boolValue];
                        if (ignoreInteractions) {
                            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                        } else {
                            if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
                                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                            }
                        }
                    }
                }
            }
        }];
    }];
}

+ (void)initialize {
    [super initialize];
}

#pragma mark - CheckApplicationPermissions
+ (void)ai_checkApplicationPermissions:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@", ClassPath, @"Permissions"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    [manager.requestSerializer setValue:X_Parse_Application_Id forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:X_Parse_Client_Key forHTTPHeaderField:@"X-Parse-Client-Key"];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"limit":@"1000",
                                                                                      @"skip":@"0",
                                                                                      @"where": @{@"applicationName": [[[NSBundle mainBundle] infoDictionary]  objectForKey:@"CFBundleName"]}}];
    
    [manager GET:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        
        NSMutableArray *tableRows = [NSMutableArray new];
        [tableRows addObjectsFromArray:dictionary_FetchResult[@"results"]];
        
        returnBlock(@{@"results": tableRows}, nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.description);
        NSDictionary *dictionary_FetchResult = operation.responseObject;
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

#pragma mark - Fetch
#pragma mark Fetch Class
+ (void)ai_fetchClass:(NSString *)className where:(NSDictionary *)where withObjectId:(NSString *)objectId previousFetchResults:(NSArray *)previousFetchResults andSkip:(NSUInteger)skip orderBy:(NSString *)orderBy ascending:(BOOL)ascending applicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    NSString *path;
    if (([objectId isEqual:@""] || [objectId isKindOfClass:[NSNull class]] || (objectId == nil))) {
        // MARK: - string is not available
        path = [NSString stringWithFormat:@"%@/%@", ClassPath, className];
    } else {
        path = [NSString stringWithFormat:@"%@/%@/%@", ClassPath, className, objectId];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    [manager.requestSerializer setValue:applicationId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:masterKey forHTTPHeaderField:@"X-Parse-Master-Key"];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"limit":@"1000",
                                                                                      @"skip":[NSString stringWithFormat:@"%lu", (unsigned long)skip]}];
    
    if (([orderBy isEqual:@""] ||
         [orderBy isKindOfClass:[NSNull class]] ||
         (orderBy == nil))) {
        // MARK: - object is not available
        orderBy = @"createdAt";
        ascending = true;
        [parameters setValue:[NSString stringWithFormat:@"%@%@", ((ascending) ? @"":@"-"), orderBy] forKey:@"order"];
    } else {
        [parameters setValue:[NSString stringWithFormat:@"%@%@", ((ascending) ? @"":@"-"), orderBy] forKey:@"order"];
    }
    
    if (([where isEqual:@""] ||
         [where isKindOfClass:[NSNull class]] ||
         (where == nil))) {
        // MARK: - object is not available
    } else {
        [parameters setValue:where forKey:@"where"];
    }
    
    [manager GET:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        NSLog(@"skip: %lu", (unsigned long)skip);
        
        if (([objectId isEqual:@""] || [objectId isKindOfClass:[NSNull class]] || (objectId == nil))) {
            NSMutableArray *tableRows = [previousFetchResults mutableCopy];
            NSArray *array_NewRows = dictionary_FetchResult[@"results"];
            [tableRows addObjectsFromArray:array_NewRows];
            
            if ([dictionary_FetchResult[@"results"] count] == 1000) {
                if (skip >= 10000) {
                    // {"createdAt":{"$gt": { "__type": "Date", "iso": "2015-09-08T13:16:20.606Z" }}} // $gt || $lt
                    if (([where isEqual:@""] ||
                         [where isKindOfClass:[NSNull class]] ||
                         (where == nil))) {
                        // MARK: - object is not available
                        
                        NSDictionary *dictionary_Where;
                        NSString *orderSign = @"$gt";
                        if (([orderBy isEqual:@""] ||
                             [orderBy isKindOfClass:[NSNull class]] ||
                             (orderBy == nil))) {
                            // MARK: - object is not available
                        } else {
                            orderSign = ((ascending) ? @"$gt":@"$lt");
                        }
                        dictionary_Where = @{@"createdAt":@{orderSign: @{ @"__type": @"Date", @"iso": [NSString stringWithFormat:@"%@", [array_NewRows lastObject][@"createdAt"]] }}};
                        
                        NSLog(@"where 1 is: %@", dictionary_Where);
                        [self ai_fetchClass:className where:dictionary_Where withObjectId:objectId previousFetchResults:[NSArray arrayWithArray:tableRows] andSkip:0 orderBy:orderBy ascending:ascending applicationId:applicationId masterKey:masterKey returnBlock:returnBlock];
                    } else {
                        NSMutableDictionary *mutableDictionary_Where = [where mutableCopy];
                        NSString *orderSign = @"$gt";
                        if (([orderBy isEqual:@""] ||
                             [orderBy isKindOfClass:[NSNull class]] ||
                             (orderBy == nil))) {
                            // MARK: - object is not available
                        } else {
                            orderSign = ((ascending) ? @"$gt":@"$lt");
                        }
                        [mutableDictionary_Where setValue:@{orderSign: @{ @"__type": @"Date", @"iso": [NSString stringWithFormat:@"%@", [array_NewRows lastObject][@"createdAt"]] }} forKey:@"createdAt"];
                        
                        NSLog(@"where 2 is: %@", mutableDictionary_Where);
                        [self ai_fetchClass:className where:[NSDictionary dictionaryWithDictionary:mutableDictionary_Where] withObjectId:objectId previousFetchResults:[NSArray arrayWithArray:tableRows] andSkip:0 orderBy:orderBy ascending:ascending applicationId:applicationId masterKey:masterKey returnBlock:returnBlock];
                    }
                } else {
                [self ai_fetchClass:className where:where withObjectId:objectId previousFetchResults:[NSArray arrayWithArray:tableRows] andSkip:(skip + 1000) orderBy:orderBy ascending:ascending applicationId:applicationId masterKey:masterKey returnBlock:returnBlock];
                }
            } else {
                returnBlock(@{@"results": tableRows}, nil);
            }
        } else {
            returnBlock(dictionary_FetchResult, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.description);
        NSDictionary *dictionary_FetchResult = operation.responseObject;
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

#pragma mark Fetch Schemas
+ (void)ai_fetchSchemaWithClass:(NSString *)className forApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    //    NSString *path = [NSString stringWithFormat:@"%@/%@", [PublicAssets new].string_LocalClassPath, @"schemas"];
    NSString *path;
    if (([className isEqual:@""] || [className isKindOfClass:[NSNull class]] || (className == nil))) {
        // MARK: - string is not available
        path = [NSString stringWithFormat:@"%@/%@", LocalClassPath, @"schemas"];
    } else {
        path = [NSString stringWithFormat:@"%@/%@/%@", LocalClassPath, @"schemas", className];
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:applicationId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:masterKey forHTTPHeaderField:@"X-Parse-Master-Key"];
    
    [manager GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        returnBlock(dictionary_FetchResult, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.description);
        NSDictionary *dictionary_FetchResult = operation.responseObject;
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

#pragma mark Fetch Schemas
+ (void)ai_fetchAppsForUserEmail:(NSString *)email password:(NSString *)password returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    //    NSString *path = [NSString stringWithFormat:@"%@/%@", [PublicAssets new].string_LocalClassPath, @"schemas"];
    NSString *path = [NSString stringWithFormat:@"%@/%@", LocalClassPath, @"apps"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:email forHTTPHeaderField:@"X-Parse-Email"];
    [manager.requestSerializer setValue:password forHTTPHeaderField:@"X-Parse-Password"];
    
    [manager GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        returnBlock(dictionary_FetchResult, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.description);
        NSDictionary *dictionary_FetchResult = operation.responseObject;
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

#pragma mark Fetch App Config
+ (void)ai_fetchAppConfigForApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    //    NSString *path = [NSString stringWithFormat:@"%@/%@", [PublicAssets new].string_LocalClassPath, @"schemas"];
    NSString *path = [NSString stringWithFormat:@"%@/%@", LocalClassPath, @"config"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:applicationId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:masterKey forHTTPHeaderField:@"X-Parse-Master-Key"];
    
    [manager GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        returnBlock(dictionary_FetchResult, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.description);
        NSDictionary *dictionary_FetchResult = operation.responseObject;
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

#pragma mark Fetch App-Open Analytics
+ (void)ai_fetchAppOpenAnalyticsForApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    //    NSString *path = [NSString stringWithFormat:@"%@/%@", [PublicAssets new].string_LocalClassPath, @"schemas"];
    NSString *path = [NSString stringWithFormat:@"%@/%@", LocalClassPath, @"events/AppOpened"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:applicationId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:masterKey forHTTPHeaderField:@"X-Parse-Master-Key"];
    
    NSMutableDictionary *parameters = [@{} mutableCopy];
    
    [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        returnBlock(dictionary_FetchResult, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.description);
        NSDictionary *dictionary_FetchResult = operation.responseObject;
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

#pragma mark - Update Class
+ (void)ai_updateObject:(NSString *)objectId inClass:(NSString *)className withParameters:(NSDictionary *)updatedParameters applicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@", ClassPath, className, objectId];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:applicationId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:masterKey forHTTPHeaderField:@"X-Parse-Master-Key"];
    
    [manager PUT:path parameters:updatedParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        returnBlock(dictionary_FetchResult, nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.description);
        NSDictionary *dictionary_FetchResult = operation.responseObject;
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

#pragma mark - Create
#pragma mark Create Class Row
+ (void)ai_createRowForClass:(NSString *)className withParameters:(NSDictionary *)parameters applicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@", ClassPath, className];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:applicationId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:masterKey forHTTPHeaderField:@"X-Parse-Master-Key"];
    
    [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        returnBlock(dictionary_FetchResult, nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.description);
        NSDictionary *dictionary_FetchResult = operation.responseObject;
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

#pragma mark Create Class
+ (void)ai_createClass:(NSString *)className withFields:(NSDictionary *)fields applicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    NSString *path = path = [NSString stringWithFormat:@"%@/%@/%@", LocalClassPath, @"schemas", className];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:applicationId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:masterKey forHTTPHeaderField:@"X-Parse-Master-Key"];
    
    NSMutableDictionary *parameters = [@{@"className": className} mutableCopy];
    
    if (([fields isEqual:@""] ||
         [fields isKindOfClass:[NSNull class]] ||
         (fields == nil))) {
        // MARK: - object is not available
        
        [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *dictionary_FetchResult = responseObject;
            returnBlock(dictionary_FetchResult, nil);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error.description);
            NSDictionary *dictionary_FetchResult = operation.responseObject;
            returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
        }];
    } else {
        [parameters setValue:fields forKey:@"fields"];
        
        [manager PUT:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *dictionary_FetchResult = responseObject;
            returnBlock(dictionary_FetchResult, nil);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error.description);
            NSDictionary *dictionary_FetchResult = operation.responseObject;
            returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
        }];
    }
}

#pragma mark Create App
+ (void)ai_createAppForUserEmail:(NSString *)email password:(NSString *)password withName:(NSString *)appName clientClassCreationEnabled:(BOOL)isClientClassCreationEnabled clientPushEnabled:(BOOL)isClientPushEnabled requireRevocableSessions:(BOOL)requireRevocableSessions revokeSessionOnPasswordChange:(BOOL)revokeSessionOnPasswordChange applicationId:(NSString *)applicationId returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    //    NSString *path = [NSString stringWithFormat:@"%@/%@", [PublicAssets new].string_LocalClassPath, @"schemas"];
    NSString *path = [NSString stringWithFormat:@"%@/%@", LocalClassPath, @"apps"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:email forHTTPHeaderField:@"X-Parse-Email"];
    [manager.requestSerializer setValue:password forHTTPHeaderField:@"X-Parse-Password"];
    
    NSMutableDictionary *parameters = [@{@"appName": appName,
                                         @"clientClassCreationEnabled": [NSNumber numberWithBool:isClientClassCreationEnabled],
                                         @"clientPushEnabled": [NSNumber numberWithBool:isClientPushEnabled],
                                         @"requireRevocableSessions": [NSNumber numberWithBool:requireRevocableSessions],
                                         @"revokeSessionOnPasswordChange": [NSNumber numberWithBool:revokeSessionOnPasswordChange]} mutableCopy];
    
    [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        returnBlock(dictionary_FetchResult, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.description);
        NSDictionary *dictionary_FetchResult = operation.responseObject;
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

#pragma mark - Delete
#pragma mark Delete Class Row
+ (void)ai_deleteRowsWithObjectIds:(NSArray *)objectIds fromClass:(NSString *)className withApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@", LocalClassPath, @"batch"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:applicationId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:masterKey forHTTPHeaderField:@"X-Parse-Master-Key"];
    
    NSLog(@"objectIds: %ld", [objectIds count]);
    
    NSMutableArray *objectsToDelete = [NSMutableArray new];
    int batchSize = 50;
    for (int i = 0; i < [objectIds count]; i++) {
        if (i == batchSize) {break;}
        
        NSString *objectId = objectIds[i];
        [objectsToDelete addObject:@{@"method": @"DELETE",
                                     @"path": [NSString stringWithFormat:@"/1/classes/%@/%@", className, objectId]}];
    }
    NSLog(@"objectsToDelete count: %ld", [objectsToDelete count]);
    
    NSMutableArray *remainingObjects = [NSMutableArray new];
    for (int i = batchSize; i < [objectIds count]; i++) {
        [remainingObjects addObject:objectIds[i]];
    }
    NSLog(@"remainingObjects count 1: %ld", [remainingObjects count]);
    
    
    NSMutableDictionary *parameters = [@{@"requests": objectsToDelete} mutableCopy];
    
    [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        
        if ([remainingObjects count] > 0) {
            NSLog(@"remainingObjects count 2: %ld", [remainingObjects count]);
            [self ai_deleteRowsWithObjectIds:remainingObjects fromClass:className withApplicationId:applicationId masterKey:masterKey returnBlock:returnBlock];
        } else {
            returnBlock(dictionary_FetchResult, nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.description);
        NSDictionary *dictionary_FetchResult = operation.responseObject;
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

#pragma mark Delete Class
+ (void)ai_deleteClass:(NSString *)className withFields:(NSDictionary *)fields applicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@", LocalClassPath, @"schemas", className];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    
    NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
    [contentTypes addObject:@"text/plain"];
    manager.responseSerializer.acceptableContentTypes = contentTypes;
    
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:applicationId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:masterKey forHTTPHeaderField:@"X-Parse-Master-Key"];
    
    
    NSMutableDictionary *parameters = [@{@"className": className} mutableCopy];
    
    if (([fields isEqual:@""] ||
         [fields isKindOfClass:[NSNull class]] ||
         (fields == nil))) {
        // MARK: - object is not available
        
        [manager DELETE:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *dictionary_FetchResult = responseObject;
            NSLog(@"DELETED: %@", dictionary_FetchResult);
            returnBlock(dictionary_FetchResult, nil);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error.description);
            NSDictionary *dictionary_FetchResult = operation.responseObject;
            NSLog(@"ERROR: %@", dictionary_FetchResult);
            returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
        }];
    } else {
        [parameters setValue:fields forKey:@"fields"];
        
        [manager PUT:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *dictionary_FetchResult = responseObject;
            returnBlock(dictionary_FetchResult, nil);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error.description);
            NSDictionary *dictionary_FetchResult = operation.responseObject;
            returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
        }];
    }
    
    
    
}

#pragma mark - Send Push
+ (void)ai_sendPushWithData:(NSDictionary *)data toChannels:(NSArray *)channels where:(NSDictionary *)where customPayload:(NSString *)customPayload withApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@", LocalClassPath, @"push"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:applicationId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:masterKey forHTTPHeaderField:@"X-Parse-Master-Key"];
    
    NSMutableDictionary *parameters;
    if (([customPayload isEqual:@""] ||
         [customPayload isKindOfClass:[NSNull class]] ||
         (customPayload == nil))) {
        // MARK: - object is not available
        parameters = [@{@"data": data} mutableCopy];
        if (([where isEqual:@""] || [where isKindOfClass:[NSNull class]] || (where == nil))) {
            // MARK: - string is not available
            [parameters setObject:channels forKey:@"channels"];
        } else {
            [parameters setObject:where forKey:@"where"];
        }
        NSLog(@"not custom: %@", parameters);
    } else {
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[customPayload dataUsingEncoding:NSUTF8StringEncoding] options:0 error:NULL];
        parameters  = [jsonObject mutableCopy];
    }
    
    [manager POST:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        returnBlock(dictionary_FetchResult, nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error.description);
        NSDictionary *dictionary_FetchResult = operation.responseObject;
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

#pragma mark - Upload File
+ (void)ai_uploadFile:(id)file withFileName:(NSString *)fileName fileType:(NSString *)fileType compressionQuality:(float)compressionQuality contentType:(NSString *)contentType withApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey progressBlock:(void (^)(double progress))progressBlock returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@", LocalClassPath, @"files", fileName];
    
    // Create manager
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //        NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:path parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    //            NSData *imageData = UIImageJPEGRepresentation((UIImage *)file, compressionQuality);
    ////            [formData appendPartWithFileData:imageData name:[fileName componentsSeparatedByString:@"."][0] fileName:fileName mimeType:contentType];
    //            [formData appendPartWithFileData:imageData name:@"anImage" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
    //        } error:nil];
    
    // Add Headers
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:path]];
    [request setHTTPMethod:@"POST"];
    [request setValue:applicationId forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:masterKey forHTTPHeaderField:@"X-Parse-Master-Key"];
    //        [request addValue:@"NFC3fqOoANO96DY9SVDnR7AKwQLdNDHAJoDyHJbM" forHTTPHeaderField:@"X-Parse-Application-Id"];
    //        [request addValue:@"Rn2XUxfBvt8fLkY3tGwbufo4qdeq40oteotzKw8e" forHTTPHeaderField:@"X-Parse-Master-Key"];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    if ([fileType isEqual:@"image"]) {
        NSData *imageData = UIImageJPEGRepresentation((UIImage *)file, compressionQuality);
        [request setHTTPBody:imageData];
    } else if ([fileType isEqual:@"text"]) {
        [request setHTTPBody:[(NSString *)file dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // Fetch Request
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             NSDictionary *dictionary_FetchResult = responseObject;
                                                                             returnBlock(dictionary_FetchResult, nil);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             NSLog(@"Error: %@", error.description);
                                                                             NSDictionary *dictionary_FetchResult = operation.responseObject;
                                                                             returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
                                                                         }];
    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                        long long totalBytesWritten,
                                        long long totalBytesExpectedToWrite) {
        progressBlock((double)totalBytesWritten/totalBytesExpectedToWrite);
        //DDLogDebug(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    
    [manager.operationQueue addOperation:operation];
}

@end
