//
//  Parse+AIHelper.h
//  Test
//
//  Created by Alexy Ibrahim on 8/24/15.
//  Copyright (c) 2015 Alexy Ibrahim. All rights reserved.
//

#import <Parse/Parse.h>

@interface Parse (AIHelper)

#pragma mark - Permission
/**@name Check Permission*/
/**Check the permissions for this local application.
 @param returnBlock The return block for the function, here you want to put your code and logic.*/
+ (void)ai_checkApplicationPermissions:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

#pragma mark - Fetch
/**@name Fetch Class*/
/**Fetch all rows of a specific class.
 @param className The class/table name to fetch.
 @param where Filter fetch result with one or many columnNames.
 @param objectId Get a specific objectId.
 @param previousFetchResults usually leave this as @[], or send an array of objects if you want them to be added to the final array.
 @param skip How many rows do you want to skip before performing the search.
 @param orderBy Order by a specific column name.
 @param ascending boolean variable specifing if the search is going to be ascending or descending order.
 @param applicationId The Parse application's ApplicationId the class belongs to.
 @param masterKey The Parse application's MasterKey the class belongs to.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note className, applicationId, masterKey are mandatory.*/
+ (void)ai_fetchClass:(NSString *)className where:(NSDictionary *)where withObjectId:(NSString *)objectId previousFetchResults:(NSArray *)previousFetchResults andSkip:(NSUInteger)skip orderBy:(NSString *)orderBy ascending:(BOOL)ascending applicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;
/**@name Fetch Schema*/
/**Fetch database schema for a specific application.
 @param className Fetch schema for a specific class, if this is nil it will fetch the schema for all the application.
 @param applicationId The Parse application's ApplicationId the class belongs to.
 @param masterKey The Parse application's MasterKey the class belongs to.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note all fields are mandatory.*/
+ (void)ai_fetchSchemaWithClass:(NSString *)className forApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;
/**@name Fetch Applications*/
/**Fetch applications for a certain Parse account.
 @param email User's Parse account email address.
 @param password User's Parse account password.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note all fields are mandatory.*/
+ (void)ai_fetchAppsForUserEmail:(NSString *)email password:(NSString *)password returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

/**@name Fetch App Config*/
/**Fetch database schema for a specific application.
 @param applicationId The Parse application's ApplicationId the configuration belongs to.
 @param masterKey The Parse application's MasterKey the configuration belongs to.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note all fields are mandatory.*/
+ (void)ai_fetchAppConfigForApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

/**@name Fetch App-Open Analytics*/
/**Fetch application open analytics for a specific application.
 @param applicationId The Parse application's ApplicationId the configuration belongs to.
 @param masterKey The Parse application's MasterKey the configuration belongs to.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note all fields are mandatory.*/
+ (void)ai_fetchAppOpenAnalyticsForApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

#pragma mark - Update
/**@name Update Class*/
/**Update a row in the class.
 @param objectId objectId of the row you want to update.
 @param className The name of the class the objectId belongs to.
 @param updatedParameters The updated parameters, not necessarily everything the row contains, only the updated ones in a @{} format.
 @param applicationId The Parse application's ApplicationId the class belongs to.
 @param masterKey The Parse application's MasterKey the class belongs to.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note all fields are mandatory.*/
+ (void)ai_updateObject:(NSString *)objectId inClass:(NSString *)className withParameters:(NSDictionary *)updatedParameters applicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

#pragma mark - Create
/**@name Create Table Row*/
/**Create a row in a specific table.
 @param className The name of the class you want to create the row for.
 @param parameters The parameters, not necessarily every single row, you want to create the row with.
 @param applicationId The Parse application's ApplicationId the class belongs to.
 @param masterKey The Parse application's MasterKey the class belongs to.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note all fields are mandatory.*/
+ (void)ai_createRowForClass:(NSString *)className withParameters:(NSDictionary *)parameters applicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

/**@name Create Table*/
/**Create/Modify a table.
 @param className The name of the class you want to create or modify.
 @param fields The column you want to create. If this is nil the table will be created with its default columns.
 @param applicationId The Parse application's ApplicationId the class belongs to.
 @param masterKey The Parse application's MasterKey the class belongs to.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note className is mandatory.*/
+ (void)ai_createClass:(NSString *)className withFields:(NSDictionary *)fields applicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

/**@name Create App*/
/**Create/Modify an application.
 @param email User's Parse account email address.
 @param password User's Parse account password.
 @param appName The application's name you want to create.
 @param isClientClassCreationEnabled Specify wether the user is able to create a table using the API.
 @param isClientPushEnabled Specify wether the user is able to send notifications using the API.
 @param requireRevocableSessions requireRevocableSessions.
 @param revokeSessionOnPasswordChange revokeSessionOnPasswordChanges.
 @param applicationId The Parse application's ApplicationId the that you want to update.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note appName is mandatory.*/
+ (void)ai_createAppForUserEmail:(NSString *)email password:(NSString *)password withName:(NSString *)appName clientClassCreationEnabled:(BOOL)isClientClassCreationEnabled clientPushEnabled:(BOOL)isClientPushEnabled requireRevocableSessions:(BOOL)requireRevocableSessions revokeSessionOnPasswordChange:(BOOL)revokeSessionOnPasswordChange applicationId:(NSString *)applicationId returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

#pragma mark - Delete
/**@name Delete Table Row*/
/**Create a row in a specific table.
 @param objectId The objectid of the row you want to delete, if this is nil the class will be deleted.
 @param className The name of the class you want to delete the row from.
 @param applicationId The Parse application's ApplicationId the class belongs to.
 @param masterKey The Parse application's MasterKey the class belongs to.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note all fields are mandatory.*/
+ (void)ai_deleteRowsWithObjectIds:(NSArray *)objectIds fromClass:(NSString *)className withApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

/**@name Delete Table*/
/**Delete a table.
 @param className The name of the class you want to delete.
 @param fields The column you want to delete. If this is nil the table will be deleted.
 @param applicationId The Parse application's ApplicationId the class belongs to.
 @param masterKey The Parse application's MasterKey the class belongs to.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note all fields are mandatory.*/
+ (void)ai_deleteClass:(NSString *)className withFields:(NSDictionary *)fields applicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

#pragma mark - Push
/**@name Send Push*/
/**Send a push notification.
 @param data The body of the push notification is 'isCustom' is false
 @param channels If channels is set and 'where' is nil, then the channels are taken into consideration
 @param where If this is is set, 'channels' are ignored and this is taken into consideration
 @param customPayload If 'isCustom' is true you will have to send the JSON String here, if its false leave this to nil
 @param applicationId The Parse application's ApplicationId the class belongs to.
 @param masterKey The Parse application's MasterKey the class belongs to.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note isCustom, data, applicationId, and masterKey are mandatory.*/
+ (void)ai_sendPushWithData:(NSDictionary *)data toChannels:(NSArray *)channels where:(NSDictionary *)where customPayload:(NSString *)customPayload withApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

#pragma mark - Upload File
/**@name Upload File*/
/**Upload a file to the Parse database.
 @param file The file you want to upload.
 @param fileName The desired file name.
 @param fileType The type of the file you're uploading, should be: image, text.
 @param compressionQuality The quality of the resulting JPEG image, expressed as a value from 0.0 to 1.0. The value 0.0 represents the maximum compression (or lowest quality) while the value 1.0 represents the least compression (or best quality)..
 @param contentType The mime-type of the file you're uploading, should be: image/jpeg, text/plain.
 @param applicationId The Parse application's ApplicationId the class belongs to.
 @param masterKey The Parse application's MasterKey the class belongs to.
 @param progressBlock The progress of the upload, value from 0.0 to 1.0.
 @param returnBlock The return block for the function, here you want to put your code and logic.
 @note all fields are mandatory.*/
+ (void)ai_uploadFile:(id)file withFileName:(NSString *)fileName fileType:(NSString *)fileType compressionQuality:(float)compressionQuality contentType:(NSString *)contentType withApplicationId:(NSString *)applicationId masterKey:(NSString *)masterKey progressBlock:(void (^)(double progress))progressBlock returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

@end
