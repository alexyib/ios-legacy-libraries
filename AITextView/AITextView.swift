//
//  AITextView.swift
//  AITextView
//
//  Created by Alexy Ibrahim on 5/8/18.
//  Copyright © 2018 Alexy Ibrahim. All rights reserved.
//

import UIKit
import BGUtilities
import Colours
import SnapKit


private enum FieldType: Int {
    case Default
    case Email
    case Password
    
    public func fieldTypeRaw() -> String {
        var fieldType: String = ""
        switch self {
        case .Default:
            fieldType = "Default"
        case .Email:
            fieldType = "Email"
        case .Password:
            fieldType = "Password"
        }
        return fieldType
    }
    public init() {
        self = .Default
    }
    public init(value: Int) {
        var result: FieldType = FieldType()
        switch value {
        case FieldType.Default.rawValue:
            result = FieldType.Default
        case FieldType.Email.rawValue:
            result = FieldType.Email
        case FieldType.Password.rawValue:
            result = FieldType.Password
        default:
            result = FieldType.Default
        }
        self = result
    }
}

@IBDesignable
public class AITextView: UITextView, UITextViewDelegate, UITextFieldDelegate {
    // --------------------Private--------------------
    @IBOutlet private var label_Placeholder:UILabel!
    @IBOutlet private var view_Bottom:UIView!
    @IBOutlet private var label_FieldType:UILabel!
    private var minimumHeight:CGFloat! = 30
    private let textView_TopMargin:CGFloat! = 22.0
    private let textView_BottomMargin:CGFloat! = 1.0
    @IBOutlet private var layoutConstraint_PlaceholderTop: NSLayoutConstraint!
    private var layoutConstraint_AITextFieldHeight: NSLayoutConstraint!
    fileprivate var fieldTypeObject: FieldType = FieldType()
    private var view_Content:UIView!
    private var isPlaceholderUp:Bool = false
    private var isCollapsed = false
    public var valueChanged:((AITextView)->Void)?
    // --------------------Public--------------------
    private var value = ""
    override public var text:String? {
        get {
            return self.value
        }
        set {
            print("AITextView - newValue: \(newValue)")
            if !(newValue?.contains("•"))! {
                print("AITextView - newValue value is not a password")
                self.value = newValue ?? ""
            } else {
                print("AITextView - newValue value is a password")
            }
            
            switch self.fieldType {
            case FieldType.Default.rawValue:
                super.text = self.value
                break
            case FieldType.Email.rawValue:
                super.text = self.value
                break
            case FieldType.Password.rawValue:
                super.text = self.getSecuredText(for: self.value)
                break
            default:
                super.text = self.value
                break
            }
        }
    }
    
    // MARK: -
    // MARK: Inspectables
    @IBInspectable
    private var placeholder:String! = "" {
        didSet {
        }
    }
    
    @IBInspectable
    private var placeholderColor:UIColor! = UIColor(fromHexString: "C7C7CD") {
        didSet {
        }
    }
    
    @IBInspectable
    private var fieldType: Int = FieldType.Default.rawValue {
        didSet {
        }
    }
    
    @IBInspectable
    public var isRequired:Bool = false {
        didSet {
        }
    }
    
    @IBInspectable
    public var isExpandable:Bool = false {
        didSet {
        }
    }
    
    @IBInspectable
    private var bottomLineColor:UIColor! = UIColor(fromHexString: "D8D8D8") {
        didSet {
        }
    }
    
    @IBInspectable
    public var debug:Bool = false {
        didSet {
        }
    }
    
    public override func layoutSubviews() {
        if self.debug { print("AITextView - layoutSubviews") }
        if self.debug { print("AITextView - text: \(self.text), value: \(self.value)") }
    }
    
    // MARK: -
    // MARK: initialization
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        if self.debug { print("AITextView - awakeFromNib") }
        if self.debug { print("AITextView - text: \(self.text), value: \(self.value)") }
        
        self.initUI()
        self.initialize()
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        if self.debug { print("AITextView - init:frame:textContainer") }
        if self.debug { print("AITextView - text: \(self.text), value: \(self.value)") }
        
        self.initUI()
        self.initialize()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if self.debug { print("AITextView - init:coder") }
        if self.debug { print("AITextView - text: \(self.text), value: \(self.value)") }
    }
    
    override public func didMoveToSuperview() {
        if self.debug { print("AITextView - didMoveToSuperview") }
        if self.debug { print("AITextView - text: \(self.text), value: \(self.value)") }
    }
    
    override public func didMoveToWindow() {
        if self.debug { print("AITextView - didMoveToWindow") }
        if self.debug { print("AITextView - text: \(self.text), value: \(self.value)") }
    }
    
    override public func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        if self.debug { print("AITextView - prepareForInterfaceBuilder") }
        if self.debug { print("AITextView - text: \(self.text), value: \(self.value)") }
        
        self.initUI()
        self.view_Content?.prepareForInterfaceBuilder()
    }
    
    // MARK: initUI
    private func initUI() {
        if self.debug { print("AITextView - initUI") }
        if self.debug { print("AITextView - text: \(self.text), value: \(self.value)") }
        
        self.xibSetup()
        
        self.view_Content.backgroundColor = self.backgroundColor
        self.backgroundColor = UIColor.clear
        
        self.view_Bottom.backgroundColor = self.bottomLineColor
        self.label_Placeholder.text = self.placeholder;
        self.label_Placeholder.textColor = self.placeholderColor
        self.label_Placeholder.font = self.font?.withSize((self.font?.pointSize ?? 14) - 4)
        
        self.fieldTypeObject = FieldType(value: self.fieldType)
        self.label_FieldType.text = self.fieldTypeObject.fieldTypeRaw()
    }
    
    // MARK: xibSetup
    private func xibSetup() {
        if self.debug { print("AITextView - xibSetup") }
        if self.debug { print("AITextView - text: \(self.text), value: \(self.value)") }
        
        let view = self.loadNibView(fromNibName: "AITextView")
        view.backgroundColor = UIColor.clear
        self.view_Content = view
        
        self.addSubview(self.view_Content)
        self.sendSubview(toBack: self.view_Content)
        self.view_Content.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
    
    // MARK: initialize
    private func initialize() {
        if self.debug { print("AITextView - initialize") }
        if self.debug { print("AITextView - text: \(self.text), value: \(self.value)") }
        
//        self.value = self.text ?? ""
        self.textContainerInset = UIEdgeInsetsMake(20, 0, 0, 0)
        
        self.delegate = self
        self.minimumHeight = CGFloat(self.height())
        if self.debug { print("AITextView - self.minimumHeight: \(self.minimumHeight)") }
        
        self.layoutConstraint_AITextFieldHeight = self.findHeightConstraint(inView: self)
        self.updateUI()
        
        self.addObserver(self, forKeyPath: "text", options: (NSKeyValueObservingOptions.new), context: nil);
        
        self.label_FieldType.removeFromSuperview()
        switch self.fieldType {
        case FieldType.Default.rawValue: break
        case FieldType.Email.rawValue: break
        case FieldType.Password.rawValue:
            self.isSecureTextEntry = true
        default: break
        }
    }
    
    
    // MARK: -
    // MARK: Delegates
    // MARK: UITextView Delegate
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if self.debug { print("AITextView - textViewDidBeginEditing") }
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text != "" {
            self.value.append(text)
        } else {
            if self.value.count > 0 {
                self.value.removeLast()
            }
        }
        
        if self.debug { print("AITextView - textView:shouldChangeTextIn, text: \(self.text), value: \(self.value)") }
        
        return true
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        self.observeValue(forKeyPath: "text", of: self, change: [NSKeyValueChangeKey.init(rawValue: self.value) : NSKeyValueObservingOptions.new], context: nil)
    }
    
    // MARK: -
    // MARK: Observer
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if self.debug { print("AITextView - observeValue, text: \(self.text), value: \(self.value)") }
        self.removeObserver(self, forKeyPath: "text")
        let textView = object as! UITextView;
        self.text = self.value
        self.addObserver(self, forKeyPath: "text", options: (NSKeyValueObservingOptions.new), context: nil);

        // --------------------Calling ValueChanged handler--------------------
        if let valueChanged = self.valueChanged {
            valueChanged(self)
        }
        
        if !self.isCollapsed {
            self.updateUI()
        }
    }
    
    // MARK: -
    // MARK: Utils - UI
    // MARK: Update UI
    private func updateUI() {
        if self.isExpandable {
            self.adjustUITextViewToFullHeight()
        } else {
            self.adjustUITextViewToMinimumHeight()
        }
        self.updatePlaceholderUI()
        self.updateBottomViewUI()
    }
    
    private func adjustUITextViewToMinimumHeight() {
        if self.debug { print("AITextView - adjustUITextViewToMinimumHeight") }
        UIView.animate(withDuration: 0.4, animations: {
            self.layoutConstraint_AITextFieldHeight.constant = self.minimumHeight
            self.layoutIfNeeded()
        }, completion: { (completed) in
            self.isCollapsed = false
        })
    }
    
    private func adjustUITextViewToFullHeight() {
        if self.debug { print("AITextView - adjustUITextViewToFullHeight: \(self.calculateAdjustedTextViewHeight())") }
        UIView.animate(withDuration: 0.4, animations: {
            self.layoutConstraint_AITextFieldHeight.constant = self.calculateAdjustedTextViewHeight()
            self.layoutIfNeeded()
        }, completion: { (completed) in
            self.isCollapsed = false
        })
    }
    
    // MARK: Update Placeholder UI
    private func updatePlaceholderUI() {
        let numberOfCharacters = self.text?.count ?? 0
        if numberOfCharacters <= 0 {
            if self.isPlaceholderUp {
                UIView.animate(withDuration: 0.4, animations: {
                    self.layoutConstraint_PlaceholderTop.constant = 22
                    //                    self.label_Placeholder.textColor = self.placeholderColor
                    
                    self.updateBottomViewUI()
                    
                    self.layoutIfNeeded()
                }, completion: { (completed) in
                    self.isPlaceholderUp = false
                })
            }
        } else {
            if numberOfCharacters >= 1 {
                if !self.isPlaceholderUp {
                    UIView.animate(withDuration: 0.4, animations: {
                        self.layoutConstraint_PlaceholderTop.constant = 0
                        //                        self.label_Placeholder.textColor = UIColor.black
                        self.layoutIfNeeded()
                    }, completion: { (completed) in
                        self.isPlaceholderUp = true
                    })
                }
            }
            
        }
    }
    
    // MARK: Update Bottom View UI
    private func updateBottomViewUI() {
        UIView.animate(withDuration: 0.4, animations: {
            let numberOfCharacters = self.text?.count ?? 0
            if numberOfCharacters <= 0 { // check if required
                if self.isRequired {
                    self.view_Bottom.backgroundColor = UIColor(fromHexString: "#DF1229")
                } else {
                    self.view_Bottom.backgroundColor = self.bottomLineColor
                }
            } else { // check validity
                if self.isValid() {
                    self.view_Bottom.backgroundColor = self.bottomLineColor
                } else {
                    self.view_Bottom.backgroundColor = UIColor(fromHexString: "#DF1229")
                }
            }
            
            self.layoutIfNeeded()
        }, completion: { (completed) in
        })
    }
    
    // MARK: Collapse
    public func collapse(animated:Bool) {
        //        self.fadeOut(withDuration: ((animated) ? 0.4:0)) { (completed) in
        //            if completed {
        //                self.layoutConstraint_AITextFieldHeight.constant = 0
        //                self.isCollapsed = true
        //            } else {
        //                self.expand()
        //            }
        //        }
        if !self.isCollapsed {
            UIView.animate(withDuration: ((animated) ? 0.4:0), animations: {
                self.layoutConstraint_AITextFieldHeight.constant = 0
                self.fadeOut()
                self.layoutIfNeeded()
            }, completion: { (completed) in
                self.isCollapsed = true
            })
        }
    }
    
    // MARK: Expand
    public func expand() {
        if self.isCollapsed {
            self.fadeIn()
            self.updateUI()
            //            self.isCollapsed = false
        }
    }
    
    // MARK: Toggle collapse/expand
    public func toggleExpandCollapse() {
        if self.isCollapsed {
            self.expand()
        } else {
            self.collapse(animated: true)
        }
    }
    
    // MARK: -
    // MARK: Utils - General
    // MARK: Loading Nib File
    private func loadNibFile(nibName: String) -> UINib {
        let bundle = Bundle(for: type(of: self) as AnyClass)
        let nib:UINib = UINib.init(nibName: nibName, bundle: bundle)
        return nib
    }
    
    /** Loads instance from nib with the same name. */
    private func loadNibView(fromNibName nibName: String) -> UIView {
        let nib = loadNibFile(nibName: nibName)
        let foundItems = nib.instantiate(withOwner: self, options: nil)
        return (foundItems.first as! UIView)
    }
    
    // MARK: FindHeightConstraint
    private func findHeightConstraint(inView view:UIView) -> NSLayoutConstraint {
        var isLayoutFound:Bool = false
        var layoutConstraint: NSLayoutConstraint!
        for constraint:NSLayoutConstraint in view.constraints {
            if constraint.firstAttribute == .height {
                layoutConstraint = constraint
                view.needsUpdateConstraints()
                isLayoutFound = true
                break
            }
        }
        if !isLayoutFound {
            layoutConstraint = NSLayoutConstraint(item: self,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: nil,
                                                  attribute: .notAnAttribute,
                                                  multiplier: 0.0,
                                                  constant: CGFloat(self.minimumHeight))
            
            self.addConstraint(layoutConstraint)
        }
        
        return layoutConstraint
    }
    
    // MARK: getSecuredText
    private func getSecuredText(for stringToConvert: String) -> String? {
        var securedString = ""
        for _ in 0..<stringToConvert.count {
            securedString = securedString + ("•")
        }
        return securedString
    }
    
    // MARK: CheckValidity
    public func isValid() -> Bool {
        switch self.fieldType {
        case FieldType.Default.rawValue:
            return true
        case FieldType.Email.rawValue:
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
            return emailTest.evaluate(with: self.text)
        case FieldType.Password.rawValue:
            return true
        default:
            return true
        }
    }
    
    private func calculateAdjustedTextViewHeight() -> CGFloat {
        if self.debug { print("AITextView - calculateAdjustedTextViewHeight") }
        
        let newSize = self.sizeThatFits(CGSize(width: CGFloat(self.width()), height: CGFloat.greatestFiniteMagnitude))
        
        var predictedTextViewHeight = newSize.height
        predictedTextViewHeight += 8
//        predictedTextViewHeight += (self.textView_TopMargin + self.textView_BottomMargin)
        if self.debug { print("AITextView - predictedTextViewHeight: \(predictedTextViewHeight)") }
        
        var adjustedHeight:CGFloat! = self.minimumHeight
        if predictedTextViewHeight > self.minimumHeight {
            adjustedHeight = predictedTextViewHeight
        }
        
        return adjustedHeight
    }
}
