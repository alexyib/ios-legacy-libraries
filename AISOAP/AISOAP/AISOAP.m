//
//  AISOAP.m
//  Test
//
//  Created by Alexy Ibrahim on 8/24/15.
//  Copyright (c) 2015 Alexy Ibrahim. All rights reserved.
//

#import "AISOAP.h"

@interface AISOAP () <NSXMLParserDelegate>
// --------------------web service-------------------- Apr 8, 2014 - 11:19 AM
@property (strong, nonatomic) NSMutableData *webData;
@property (strong, nonatomic) NSXMLParser *xmlParser;
@property (strong, nonatomic) NSString *finalData;
@property (strong, nonatomic) NSMutableString *nodeContent;
@property (strong, nonatomic) NSString *string_ElementName; // main tag name
@property (strong, nonatomic) NSMutableArray *array_ElementNames; // tags names
@property (strong, nonatomic) NSMutableDictionary *dictionary_Item; // each main tag is saved in a dictionary
@property (strong, nonatomic) NSMutableArray *array_Result; // array storing search results

@property (strong, nonatomic) NSString *soapAction;

@property (strong, nonatomic) void (^block)(NSArray *webServiceResult);
@property (strong, nonatomic) void (^failblock)(NSError *error);
@end

@implementation AISOAP

+ (void)ai_connectToWebServiceSOAP:(NSString *)soapMessage webServiceLocation:(NSString *)webServiceLocation contentType:(NSString *)contentType soapAction:(NSString *)soapAction httpMethod:(NSString *)method
                        success:(void (^)(NSArray *webServiceResult))success
                        failure:(void (^)(NSError *error))failure
{
    dispatch_async(dispatch_get_main_queue(), ^{
        AISOAP *webService = [[AISOAP alloc] init];
        [webService ai_connectToWebServiceSOAP:soapMessage webServiceLocation:webServiceLocation contentType:contentType soapAction:soapAction httpMethod:method success:success failure:failure];
    });
}

- (void)ai_connectToWebServiceSOAP:(NSString *)soapMessage webServiceLocation:(NSString *)webServiceLocation contentType:(NSString *)contentType soapAction:(NSString *)soapAction httpMethod:(NSString *)method
                        success:(void (^)(NSArray *feedItems))success
                        failure:(void (^)(NSError *error))failure
{
    // --------------------creating references-------------------- Apr 8, 2014 - 12:25 PM
    self.block = [success copy];
    self.failblock = [failure copy];
    self.soapAction = soapAction;
    
    // --------------------setting up web service variables-------------------- Apr 8, 2014 - 12:25 PM
    NSString *soapFormat = soapMessage;
    
    //    NSLog(@"The request format is %@",soapFormat);
    
    NSURL *locationOfWebService = [NSURL URLWithString:webServiceLocation];
    
    //    NSLog(@"web url = %@",locationOfWebService);
    
    NSMutableURLRequest *theRequest = [[NSMutableURLRequest alloc]initWithURL:locationOfWebService];
    
    NSString *msgLength = [NSString stringWithFormat:@"%lu",(unsigned long)[soapFormat length]];
    
    [theRequest addValue:contentType forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue:soapAction forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue:msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:method];
    //the below encoding is used to send data over the net
    [theRequest setHTTPBody:[soapFormat dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSURLConnection *connect = [[NSURLConnection alloc]initWithRequest:theRequest delegate:self];
    
    if (connect) {
        self.webData = [[NSMutableData alloc] init];
        // --------------------Start Activity Indicator-------------------- Mar 18, 2014 - 5:11 PM
    }
    else {
        NSLog(@"No Connection established");
    }
    
    self.nodeContent = [[NSMutableString alloc] init];
    self.array_Result = [[NSMutableArray alloc] init];
}

#pragma mark - NSURLConnection delegate methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.webData setLength: 0];
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.webData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"ERROR with theConenction");
    error.accessibilityLabel = @"Connection Error";
    self.failblock(error);
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"DONE fetching %@. Received Bytes: %lu", [[self.soapAction componentsSeparatedByString:@"/"] lastObject], (unsigned long)[self.webData length]);
    //	NSString *theXML = [[NSString alloc] initWithBytes: [self.webData mutableBytes] length:[self.webData length] encoding:NSUTF8StringEncoding];
    //	NSLog(@"%@",theXML);
    
    self.xmlParser = [[NSXMLParser alloc]initWithData:self.webData];
    [self.xmlParser setDelegate: self];
    self.xmlParser.accessibilityLabel = @"normal";
    // [xmlParser setShouldResolveExternalEntities: YES];
    [self.xmlParser parse];
}

#pragma mark - XML Parser Delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    //    NSLog(@"didStartElement");
    //    NSLog(@"elementName: %@", elementName);
    //    NSLog(@"namespaceURI: %@", namespaceURI);
    //    NSLog(@"qualifiedName: %@", qualifiedName);
    //    NSLog(@"attributeDict: %@\n\n", attributeDict);
    
    // --------------------reset node-------------------- Apr 14, 2014 - 2:21 PM
    self.nodeContent = [NSMutableString string];
    
    if ([parser.accessibilityLabel isEqualToString:@"normal"]) {
        if ([elementName isEqualToString:@"xs:element"] && ([attributeDict count] == 1)) {
            self.string_ElementName = [attributeDict objectForKey:@"name"];
        } else if ([elementName isEqualToString:@"xs:sequence"]) {
            // --------------------start adding element names-------------------- Apr 3, 2014 - 4:48 PM
            self.array_ElementNames = [[NSMutableArray alloc] init];
            parser.accessibilityLabel = @"sequence";
        } else if ([elementName isEqualToString:self.string_ElementName]) {
            self.dictionary_Item = [[NSMutableDictionary alloc] init];
            parser.accessibilityLabel = @"storing";
        }
    } else if ([parser.accessibilityLabel isEqualToString:@"sequence"]) {
        if ([elementName isEqualToString:@"xs:element"]) {
            // --------------------adding element names-------------------- Apr 3, 2014 - 4:49 PM
            [self.array_ElementNames addObject:[attributeDict objectForKey:@"name"]];
        }
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    //    NSLog(@"foundCharacters");
    //    NSLog(@"string: %@\n\n", self.nodeContent);
    
    //	self.nodeContent = [NSMutableString stringWithString:string];
    // --------------------sometimes foundCharacters method is called several times one after another (if for example the string contained an "&"), if we initialized nodeContent = to the foundCharacters string -> the string will be cleared/reset each time it enters this function, but instead we want it to append the several found strings-------------------- Apr 14, 2014 - 2:22 PM
    [self.nodeContent appendString:string]; // [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    //    NSLog(@"didEndElement");
    //    NSLog(@"elementName: %@", elementName);
    //    NSLog(@"namespaceURI: %@", namespaceURI);
    //    NSLog(@"qName: %@\n\n", qName);
    
    if ([parser.accessibilityLabel isEqualToString:@"sequence"]) {
        if ([elementName isEqualToString:@"xs:sequence"]) {
            // --------------------switching sequence flag to normal-------------------- Apr 3, 2014 - 4:48 PM
            parser.accessibilityLabel = @"normal";
        }
    } else if ([parser.accessibilityLabel isEqualToString:@"storing"]) {
        // --------------------end element tag name reached--------------------
        if ([elementName isEqualToString:self.string_ElementName]) {
            [self.array_Result addObject:self.dictionary_Item];
            parser.accessibilityLabel = @"normal";
            return;
        }
        
        // --------------------checking if tag exist-------------------- Apr 3, 2014 - 4:46 PM
        for (int i = 0; i < [self.array_ElementNames count]; i++) {
            if ([elementName isEqualToString:[self.array_ElementNames objectAtIndex:i]]) {
                [self.dictionary_Item setObject:self.nodeContent forKey:elementName];
                break;
            }
        }
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    //    NSLog(@"self.string_ElementName: %@", self.string_ElementName);
    //    NSLog(@"array_ElementNames: %@", self.array_ElementNames);
    NSLog(@"DATA count #%lu", (unsigned long)[self.array_Result count]);
    self.block(self.array_Result);
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSLog(@"ERROR parsing data");
    parseError.accessibilityLabel = @"Parsing Error";
    self.failblock(parseError);
    [parser abortParsing];
}

@end
