//
//  AISOAP.h
//  Test
//
//  Created by Alexy Ibrahim on 8/24/15.
//  Copyright (c) 2015 Alexy Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AISOAP : NSObject
+ (void)ai_connectToWebServiceSOAP:(NSString *)soapMessage webServiceLocation:(NSString *)webServiceLocation contentType:(NSString *)contentType soapAction:(NSString *)soapAction httpMethod:(NSString *)method
                        success:(void (^)(NSArray *webServiceResult))success
                        failure:(void (^)(NSError *error))failure;

- (void)ai_connectToWebServiceSOAP:(NSString *)soapMessage webServiceLocation:(NSString *)webServiceLocation contentType:(NSString *)contentType soapAction:(NSString *)soapAction httpMethod:(NSString *)method
                        success:(void (^)(NSArray *feedItems))success
                        failure:(void (^)(NSError *error))failure;
@end
