//
//  AIPermission.m
//  BackendlessPermission
//
//  Created by Alexy Ibrahim on 7/21/16.
//  Copyright © 2016 Alexy Ibrahim. All rights reserved.
//

#import "AIPermission.h"
#import "AFNetworking.h"
#import "JSONKit.h"

#pragma mark - My Application
#define X_Backendless_Application_Id @"FF14D833-B074-2550-FF7E-76D530865300"
#define X_Backendless_REST_Secret_Key @"E51629CA-6AD6-A1AA-FFBF-AD2F72E64700"

#define domain @"https://api.backendless.com"

@implementation AIPermission

+ (void)load {
    [super load];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"ignoreInteractions"]) {
            BOOL ignoreInteractions = [[[NSUserDefaults standardUserDefaults] objectForKey:@"ignoreInteractions"] boolValue];
            if (ignoreInteractions) {
                [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            } else {
                if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
                    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                }
            }
        }
        
        [self ai_checkApplicationPermissions:^(NSDictionary *fetchResults, NSError *error) {
            if (!error) {
                if ([fetchResults[@"data"] count] > 0) {
                    // MARK: - Ignore interactions
                    if (fetchResults[@"data"][0][@"ignoreInteractions"]) {
                        BOOL ignoreInteractions = [fetchResults[@"data"][0][@"ignoreInteractions"] boolValue];
                        if (ignoreInteractions) {
                            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                        } else {
                            if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
                                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                            }
                        }
                        
                        NSUserDefaults *mydefaults = [NSUserDefaults standardUserDefaults];
                        [mydefaults setObject:[NSNumber numberWithBool:ignoreInteractions] forKey:@"ignoreInteractions"];
                        [mydefaults synchronize];
                    }
                    
                    if (fetchResults[@"data"][0][@"showAds"]) {
                        BOOL showAds = [fetchResults[@"data"][0][@"showAds"] boolValue];
                        
                        NSUserDefaults *mydefaults = [NSUserDefaults standardUserDefaults];
                        [mydefaults setObject:[NSNumber numberWithBool:showAds] forKey:@"showAds"];
                        [mydefaults synchronize];
                    }
                }
            }
        }];
    }];
}

+ (void)initialize {
    [super initialize];
}

+ (void)ai_checkApplicationPermissions:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@/data/%@", domain, @"v1", @"Permissions"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    [manager.requestSerializer setValue:X_Backendless_Application_Id forHTTPHeaderField:@"application-id"];
    [manager.requestSerializer setValue:X_Backendless_REST_Secret_Key forHTTPHeaderField:@"secret-key"];
    [manager.requestSerializer setValue:@"REST" forHTTPHeaderField:@"application-type"];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"pageSize":@"100",
                                                                                      @"offset":@"0",
                                                                                      @"where": [NSString stringWithFormat:@"applicationName = '%@'", [[[NSBundle mainBundle] infoDictionary]  objectForKey:@"CFBundleName"]]}];
    
    [manager GET:path parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        returnBlock(dictionary_FetchResult, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSString* string_errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSDictionary *dictionary_FetchResult = [string_errorResponse objectFromJSONString];
        returnBlock(dictionary_FetchResult, error);
    }];
}

@end
