//
//  EmptyDataSet.h
//  EmptyDataSet
//
//  Created by Alexy on 2/6/15.
//  Copyright (c) 2015 Alexy Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class EmptyDataSetView;

@interface EmptyDataSet : NSObject
- (void)registerForEmptySetWithTableView:(UITableView *)tableView inViewController:(UIViewController *)viewController;
- (void)registerForEmptySetWithCollectionView:(UICollectionView *)collectionView;
@property (strong, nonatomic) EmptyDataSetView *emptyDataSetView;
@end

@interface EmptyDataSetView : UIView
@property (strong, nonatomic) EmptyDataSet *emptyDataSet;
@property (nonatomic, readonly) UIView *contentView;
@property (nonatomic, readonly) UILabel *titleLabel;
@property (nonatomic, readonly) UILabel *detailLabel;
@property (nonatomic, readonly) UIImageView *imageView;
@property (nonatomic, readonly) UIButton *button;
@property (nonatomic, strong) UIView *customView;

@property (nonatomic, assign) CGPoint offset;
@property (nonatomic, assign) CGFloat verticalSpace;

- (void)removeAllSubviews;
@end
