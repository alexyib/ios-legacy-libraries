//
//  EmptyDataSet.m
//  EmptyDataSet
//
//  Created by Alexy on 2/6/15.
//  Copyright (c) 2015 Alexy Ibrahim. All rights reserved.
//

#import "EmptyDataSet.h"

@interface EmptyDataSetView ()
@end

@interface EmptyDataSet () <UIGestureRecognizerDelegate>
@property (strong, nonatomic) UITableView *tableView_Main;
@property (strong, nonatomic) UICollectionView *collectionView_Main;
@property (strong, nonatomic) UIViewController *superViewController;
@property (nonatomic) BOOL bool_isTableView;
@end


@implementation EmptyDataSet

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        if (!self.emptyDataSetView)
        {
            self.emptyDataSetView = [EmptyDataSetView new];
            self.emptyDataSetView.backgroundColor = [UIColor whiteColor];
            self.emptyDataSetView.emptyDataSet = self;
            self.emptyDataSetView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
            self.emptyDataSetView.userInteractionEnabled = YES;
            self.emptyDataSetView.hidden = true;
            
            UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
            gesture.delegate = self;
            [self.emptyDataSetView addGestureRecognizer:gesture];
        }
    }
    return self;
}

#pragma mark - RegisterForEmptySet
- (void)registerForEmptySetWithTableView:(UITableView *)tableView inViewController:(UIViewController *)viewController {
    self.bool_isTableView = true;
    self.tableView_Main = tableView;
    self.superViewController = viewController;
    
    [self.tableView_Main addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionOld context:NULL];
}

- (void)registerForEmptySetWithCollectionView:(UICollectionView *)collectionView {
    self.bool_isTableView = false;
    self.collectionView_Main = collectionView;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"contentSize"]) {
        [self checkIf:self.tableView_Main isEmpty:^(BOOL isEmpty, NSError *error) {
            if (!error) {
                if (isEmpty) {
                    [self.superViewController.view addSubview:self.emptyDataSetView];
                    self.emptyDataSetView.hidden = false;
                } else {
                    [self.emptyDataSetView removeFromSuperview];
                }
            } else {
                NSLog(@"error: %@", error);
            }
        }];
    }
}

#pragma mark - CheckIf UITableView/UICollectionView isEmpty
- (void)checkIf:(id)view isEmpty:(void (^)(BOOL isEmpty, NSError *error))returnBlock {
    if ([view isKindOfClass:[UITableView class]]) {
        UITableView *tableView = (UITableView *)view;
        for (int i = 0; i < tableView.numberOfSections; i++) {
            if ([tableView numberOfRowsInSection:i] > 0) {
                returnBlock (false, nil);
                return;
            }
        }
        returnBlock (true, nil);
    } else if ([view isKindOfClass:[UICollectionView class]]) {
        UICollectionView *collectionView = (UICollectionView *)view;
        for (int i = 0; i < collectionView.numberOfSections; i++) {
            if ([collectionView numberOfItemsInSection:i] > 0) {
                returnBlock (false, nil);
                return;
            }
        }
        returnBlock (true, nil);
    }
    returnBlock (false, [NSError errorWithDomain:@"view type is not supported" code:25399 userInfo:@{@"description": @"the provided view's class is not compatible"}]);
}

//- (EmptyDataSetView *)emptyDataSetView
//{
//    if (!self.emptyDataSetView)
//    {
//        self.emptyDataSetView = [EmptyDataSetView new];
////        self.emptyDataSetView.emptyDataSet = self;
////        self.emptyDataSetView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
//        self.emptyDataSetView.userInteractionEnabled = YES;
//        self.emptyDataSetView.hidden = YES;
//        
//        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
//        gesture.delegate = self;
//        [self.emptyDataSetView addGestureRecognizer:gesture];
//    }
//    return self.emptyDataSetView;
//}

#pragma mark - TapGesture Handler
- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture {
    NSLog(@"singleTapGestureCaptured");
}

@end

@implementation EmptyDataSetView
@synthesize contentView = _contentView;
@synthesize titleLabel = _titleLabel, detailLabel = _detailLabel, imageView = _imageView, button = _button;

#pragma mark - Initialization Methods

- (instancetype)init
{
    self =  [super init];
    if (self) {
        [self addSubview:self.contentView];
    }
    return self;
}

- (void)didMoveToSuperview
{
    self.frame = self.superview.bounds;
    
    [UIView animateWithDuration:0.25
                     animations:^{_contentView.alpha = 1.0;}
                     completion:NULL];
}

#pragma mark - Getters

- (UIView *)contentView
{
    if (!_contentView)
    {
        _contentView = [[UIView alloc] initWithFrame:self.frame];
        _contentView.translatesAutoresizingMaskIntoConstraints = NO;
        _contentView.backgroundColor = [UIColor clearColor];
        _contentView.userInteractionEnabled = YES;
        _contentView.alpha = 0;
    }
    return _contentView;
}

- (UIImageView *)imageView
{
    if (!_imageView)
    {
        _imageView = [UIImageView new];
        _imageView.translatesAutoresizingMaskIntoConstraints = NO;
        _imageView.backgroundColor = [UIColor clearColor];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        _imageView.userInteractionEnabled = NO;
        _imageView.accessibilityLabel = @"empty set background image";
        
        [_contentView addSubview:_imageView];
    }
    return _imageView;
}

- (UILabel *)titleLabel
{
    if (!_titleLabel)
    {
        _titleLabel = [UILabel new];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.backgroundColor = [UIColor clearColor];
        
        _titleLabel.font = [UIFont systemFontOfSize:27.0];
        _titleLabel.textColor = [UIColor colorWithWhite:0.6 alpha:1.0];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _titleLabel.numberOfLines = 2;
        _titleLabel.accessibilityLabel = @"empty set title";
        
        [_contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UILabel *)detailLabel
{
    if (!_detailLabel)
    {
        _detailLabel = [UILabel new];
        _detailLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _detailLabel.backgroundColor = [UIColor clearColor];
        
        _detailLabel.font = [UIFont systemFontOfSize:17.0];
        _detailLabel.textColor = [UIColor colorWithWhite:0.6 alpha:1.0];
        _detailLabel.textAlignment = NSTextAlignmentCenter;
        _detailLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _detailLabel.numberOfLines = 0;
        _detailLabel.accessibilityLabel = @"empty set detail label";
        
        [_contentView addSubview:_detailLabel];
    }
    return _detailLabel;
}

- (UIButton *)button
{
    if (!_button)
    {
        _button = [UIButton buttonWithType:UIButtonTypeCustom];
        _button.translatesAutoresizingMaskIntoConstraints = NO;
        _button.backgroundColor = [UIColor clearColor];
        _button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        _button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _button.accessibilityLabel = @"empty set button";
        
        [_button addTarget:self action:@selector(didTapButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [_contentView addSubview:_button];
    }
    return _button;
}

- (BOOL)canShowImage {
    return (_imageView.image && _imageView.superview);
}

- (BOOL)canShowTitle {
    return (_titleLabel.attributedText.string.length > 0 && _titleLabel.superview);
}

- (BOOL)canShowDetail {
    return (_detailLabel.attributedText.string.length > 0 && _detailLabel.superview);
}

- (BOOL)canShowButton {
    return ([_button attributedTitleForState:UIControlStateNormal].string.length > 0 && _button.superview);
}


#pragma mark - Setters

- (void)setCustomView:(UIView *)view
{
    if (_customView) {
        [_customView removeFromSuperview];
        _customView = nil;
    }
    
    if (!view) {
        return;
    }
    
    _customView = view;
    _customView.translatesAutoresizingMaskIntoConstraints = !CGRectIsEmpty(view.frame);
    
    [_contentView addSubview:_customView];
}


#pragma mark - Action Methods

- (void)didTapButton:(id)sender
{
    SEL selector = NSSelectorFromString(@"didTapDataButton:");
    if ([self.emptyDataSet.superViewController respondsToSelector:selector]) {
        [self.emptyDataSet.superViewController performSelector:selector withObject:sender afterDelay:0.0f];
    }
}

- (void)removeAllSubviews
{
    [self.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    _titleLabel = nil;
    _detailLabel = nil;
    _imageView = nil;
    _button = nil;
    _customView = nil;
}

- (void)removeAllConstraints
{
    [self removeConstraints:self.constraints];
    [self.contentView removeConstraints:self.contentView.constraints];
}


#pragma mark - UIView Constraints & Layout Methods

- (void)updateConstraintsIfNeeded
{
    [super updateConstraintsIfNeeded];
}

- (void)updateConstraints
{
    // Cleans up any constraints
    [self removeAllConstraints];
    
    NSMutableDictionary *views = [NSMutableDictionary dictionary];
    [views setObject:self forKey:@"self"];
    [views setObject:self.contentView forKey:@"contentView"];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[self]-(<=0)-[contentView]"
                                                                 options:NSLayoutFormatAlignAllCenterY metrics:nil views:views]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[self]-(<=0)-[contentView]"
                                                                 options:NSLayoutFormatAlignAllCenterX metrics:nil views:views]];
    
    // If a custom offset is available, we modify the contentView's constraints constants
    if (!CGPointEqualToPoint(self.offset, CGPointZero) && self.constraints.count == 4) {
        NSLayoutConstraint *vConstraint = self.constraints[1];
        NSLayoutConstraint *hConstraint = [self.constraints lastObject];
        
        // the values must be inverted to follow the up-bottom and left-right directions
        vConstraint.constant = self.offset.y*-1;
        hConstraint.constant = self.offset.x*-1;
    }
    
    if (_customView) {
        [views setObject:_customView forKey:@"customView"];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[customView]|" options:0 metrics:nil views:views]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[customView]|" options:0 metrics:nil views:views]];
        
        // Skips from any further configuration
        return [super updateConstraints];;
    }
    
    CGFloat width = CGRectGetWidth(self.frame) ? : CGRectGetWidth([UIScreen mainScreen].bounds);
    NSNumber *padding =  [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone ? @20 : @(roundf(width/16.0));
    NSNumber *imgWidth = @(roundf(_imageView.image.size.width));
    NSNumber *imgHeight = @(roundf(_imageView.image.size.height));
    NSNumber *trailing = @(roundf((width-[imgWidth floatValue])/2.0));
    
    NSDictionary *metrics = NSDictionaryOfVariableBindings(padding,trailing,imgWidth,imgHeight);
    
    // Since any element could be missing from displaying, we need to create a dynamic string format
    NSMutableArray *verticalSubviews = [NSMutableArray new];
    
    // Assign the image view's horizontal constraints
    if (_imageView.superview) {
        [views setObject:_imageView forKey:@"imageView"];
        [verticalSubviews addObject:@"[imageView(imgHeight)]"];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-trailing-[imageView(imgWidth)]-trailing-|"
                                                                                 options:0 metrics:metrics views:views]];
    }
    
    // Assign the title label's horizontal constraints
    if ([self canShowTitle]) {
        [views setObject:_titleLabel forKey:@"titleLabel"];
        [verticalSubviews addObject:@"[titleLabel]"];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[titleLabel]-padding-|"
                                                                                 options:0 metrics:metrics views:views]];
    }
    // or removes from its superview
    else {
        [_titleLabel removeFromSuperview];
        _titleLabel = nil;
    }
    
    // Assign the detail label's horizontal constraints
    if ([self canShowDetail]) {
        [views setObject:_detailLabel forKey:@"detailLabel"];
        [verticalSubviews addObject:@"[detailLabel]"];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[detailLabel]-padding-|"
                                                                                 options:0 metrics:metrics views:views]];
    }
    // or removes from its superview
    else {
        [_detailLabel removeFromSuperview];
        _detailLabel = nil;
    }
    
    // Assign the button's horizontal constraints
    if (_button.superview) {
        [views setObject:_button forKey:@"button"];
        [verticalSubviews addObject:@"[button]"];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[button]-padding-|"
                                                                                 options:0 metrics:metrics views:views]];
    }
    // or removes from its superview
    else {
        [_button removeFromSuperview];
        _button = nil;
    }
    
    
    // Build the string format for the vertical constraints, adding a margin between each element. Default is 11.
    NSString *verticalFormat = [verticalSubviews componentsJoinedByString:[NSString stringWithFormat:@"-%.f-", self.verticalSpace ?: 11]];
    
    // Assign the vertical constraints to the content view
    if (verticalFormat.length > 0) {
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|%@|", verticalFormat]
                                                                                 options:0 metrics:metrics views:views]];
    }
    
    [super updateConstraints];
}

@end