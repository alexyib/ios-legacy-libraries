//
//  Parse+AIPermission.m
//  Test
//
//  Created by Alexy Ibrahim on 8/24/15.
//  Copyright (c) 2015 Alexy Ibrahim. All rights reserved.
//

#import "Parse+AIPermission.h"
#import <AfNetworking/AFNetworking.h>
#import <JSONKit/JSONKit.h>

#pragma mark - My Application
#define X_Parse_Application_Id @"jPKq1YQm1rwPqKITM8CUhquPHBpKt0uXHgxFTL7k"
#define X_Parse_Client_Key @"pVNe6M2AdTJkCZBQ6sx8dGcCfiaNXbZPyd4IkD8J"

#define ClassPath @"https://api.parse.com/1/classes"

@implementation Parse (AIPermission)

+ (void)load {
    [super load];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self ai_checkApplicationPermissions:^(NSDictionary *fetchResults, NSError *error) {
            if (!error) {
                if ([fetchResults[@"results"] count] > 0) {
                    // MARK: - Ignore interactions
                    if (fetchResults[@"results"][0][@"ignoreInteractions"]) {
                        BOOL ignoreInteractions = [fetchResults[@"results"][0][@"ignoreInteractions"] boolValue];
                        if (ignoreInteractions) {
                            [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                        } else {
                            if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
                                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                            }
                        }
                    }
                }
            }
        }];
    }];
}

+ (void)initialize {
    [super initialize];
}

#pragma mark - CheckApplicationPermissions
+ (void)ai_checkApplicationPermissions:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    NSString *path = [NSString stringWithFormat:@"%@/%@", ClassPath, @"Permissions"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    [manager.requestSerializer setValue:X_Parse_Application_Id forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:X_Parse_Client_Key forHTTPHeaderField:@"X-Parse-Client-Key"];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"limit":@"1000",
                                                                                      @"skip":@"0",
                                                                                      @"where": @{@"applicationName": [[[NSBundle mainBundle] infoDictionary]  objectForKey:@"CFBundleName"]}}];
    
    [manager GET:path parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dictionary_FetchResult = responseObject;
        
        NSMutableArray *tableRows = [NSMutableArray new];
        [tableRows addObjectsFromArray:dictionary_FetchResult[@"results"]];
        
        returnBlock(@{@"results": tableRows}, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error.description);
        NSString* string_errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSDictionary *dictionary_FetchResult = [string_errorResponse objectFromJSONString];
        returnBlock(dictionary_FetchResult, [NSError errorWithDomain:@"A.I" code:1221990 userInfo:@{@"reason": error.description}]);
    }];
}

@end
