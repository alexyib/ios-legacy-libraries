//
//  ScrollView.m
//  St. Rafka
//
//  Created by Alexy on 4/7/14.
//  Copyright (c) 2014 Alexy Ibrahim. All rights reserved.
//

#import "ScrollView.h"

@implementation ScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// UIScrollView subclass
- (void)setContentSize:(CGSize)contentSize {
    if (contentSize.height > 0)
        [super setContentSize:contentSize];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
