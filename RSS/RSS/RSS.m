//
//  RSS.m
//
//  Created by Alexy on 4/26/14.
//  Copyright (c) 2014 Alexy Ibrahim. All rights reserved.
//

#import "RSS.h"

#import <AFHTTPRequestOperation.h>
#import <AFURLResponseSerialization.h>

@interface RSS () <NSXMLParserDelegate>
// --------------------RSS-------------------- Apr 26, 2014 - 11:24 PM
@property (strong, nonatomic) NSXMLParser *xmlParser;
@property (strong, nonatomic) NSString *finalData;
@property (strong, nonatomic) NSMutableString *nodeContent;

@property (strong, nonatomic) NSMutableDictionary *dictionary_Channel;
@property (strong, nonatomic) RSSItem *rssItem;
@property (strong, nonatomic) NSMutableArray *array_Result; // array storing search results

@property (strong, nonatomic) NSString *soapAction;

@property (strong, nonatomic) void (^block)(NSDictionary *channel, NSArray *feedItems);
@property (strong, nonatomic) void (^failblock)(NSError *error);

@end

@implementation RSS

#pragma mark - Parser

+ (void)fetchRSSFeedForURLString:(NSString *)rssFeedString
                       success:(void (^)(NSDictionary *channel, NSArray *feedItems))success
                       failure:(void (^)(NSError *error))failure
{
    dispatch_async(dispatch_get_main_queue(), ^{
        RSS *rssParser = [[RSS alloc] init];
        [rssParser fetchRSSFeedForURLString:rssFeedString success:success failure:failure];
    });
}

- (void)fetchRSSFeedForURLString:(NSString *)rssFeedString
                       success:(void (^)(NSDictionary *channel, NSArray *feedItems))success
                       failure:(void (^)(NSError *error))failure
{
    // --------------------creating references-------------------- Apr 27, 2014 - 12:46 AM
    self.block = [success copy];
    self.failblock = [failure copy];
    
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[rssFeedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:req];
    
    operation.responseSerializer = [[AFXMLParserResponseSerializer alloc] init];
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/xml", @"text/xml",@"application/rss+xml", @"application/atom+xml", nil];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.xmlParser = (NSXMLParser *)responseObject;
        [self.xmlParser setDelegate:self];
        self.xmlParser.accessibilityLabel = @"normal";
        [self.xmlParser parse];
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                         error.accessibilityLabel = @"Connection Error";
                                         self.failblock(error);
                                     }];
    
    self.nodeContent = [[NSMutableString alloc] init];
    self.array_Result = [[NSMutableArray alloc] init];
    
    [operation start];
}

#pragma mark - NSXMLParser Delegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
//    NSLog(@"didStartElement");
//    NSLog(@"elementName: %@", elementName);
//    NSLog(@"namespaceURI: %@", namespaceURI);
//    NSLog(@"qualifiedName: %@", qualifiedName);
//    NSLog(@"attributeDict: %@\n\n", attributeDict);
    
    // --------------------reset node-------------------- Apr 14, 2014 - 2:21 PM
    self.nodeContent = [NSMutableString string];
    
    if ([parser.accessibilityLabel isEqualToString:@"normal"]) {
        if ([elementName isEqualToString:@"channel"]) {
            self.dictionary_Channel = [[NSMutableDictionary alloc] init];
            parser.accessibilityLabel = @"storeChannel";
        } else if ([elementName isEqualToString:@"item"] || [elementName isEqualToString:@"entry"]) {
            self.rssItem = [[RSSItem alloc] init];
            parser.accessibilityLabel = @"storeItem";
        }
    } else if ([parser.accessibilityLabel isEqualToString:@"storeChannel"]) {
        if ([elementName isEqualToString:@"item"] || [elementName isEqualToString:@"entry"]) {
            self.rssItem = [[RSSItem alloc] init];
            parser.accessibilityLabel = @"storeItem";
        }
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
//    NSLog(@"foundCharacters");
//    [self.nodeContent appendString:string];
//    NSLog(@"string: %@\n\n", self.nodeContent);
    
    //	self.nodeContent = [NSMutableString stringWithString:string];
    // --------------------sometimes foundCharacters method is called several times one after another (if for example the string contained an "&"), if we initialized nodeContent = to the foundCharacters string -> the string will be cleared/reset each time it enters this function, but instead we want it to append the several found strings-------------------- Apr 14, 2014 - 2:22 PM
    [self.nodeContent appendString:string];
    
//    NSLog(@"self.nodeContent: %@", self.nodeContent);
//    self.nodeContent = [NSMutableString stringWithString:[self.nodeContent stringByReplacingOccurrencesOfString:@"\n" withString:@""]];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
//    NSLog(@"didEndElement");
//    NSLog(@"elementName: %@", elementName);
//    NSLog(@"namespaceURI: %@", namespaceURI);
//    NSLog(@"qName: %@\n\n", qName);
    
	if ([parser.accessibilityLabel isEqualToString:@"storeChannel"]) {
        [self.dictionary_Channel setObject:self.nodeContent forKey:elementName];
    } else if ([parser.accessibilityLabel isEqualToString:@"storeItem"]) {
        // --------------------end element tag name reached--------------------
        if ([elementName isEqualToString:@"item"] || [elementName isEqualToString:@"entry"]) {
            [self.array_Result addObject:self.rssItem];
            parser.accessibilityLabel = @"normal";
            return;
        }
        
        [self.rssItem.dictionary_Item setObject:[self.nodeContent stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  forKey:elementName];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    //    NSLog(@"self.string_ElementName: %@", self.string_ElementName);
    //    NSLog(@"array_ElementNames: %@", self.array_ElementNames);
    NSLog(@"DATA count #%lu", (unsigned long)[self.array_Result count]);
    self.block(self.dictionary_Channel, self.array_Result);
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSLog(@"ERROR parsing data");
    parseError.accessibilityLabel = @"Parsing Error";
    self.failblock(parseError);
    [parser abortParsing];
}

@end
