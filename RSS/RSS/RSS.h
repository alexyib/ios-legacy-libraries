//
//  RSS.h
//
//  Created by Alexy on 4/26/14.
//  Copyright (c) 2014 Alexy Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSItem.h"

@interface RSS : NSObject
+ (void)fetchRSSFeedForURLString:(NSString *)rssFeedString
                         success:(void (^)(NSDictionary *channel, NSArray *feedItems))success
                         failure:(void (^)(NSError *error))failure;

- (void)fetchRSSFeedForURLString:(NSString *)rssFeedString
                         success:(void (^)(NSDictionary *channel, NSArray *feedItems))success
                         failure:(void (^)(NSError *error))failure;
@end
