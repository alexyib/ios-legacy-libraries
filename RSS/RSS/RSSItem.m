//
//  RSSItem.m
//  St. Rafka
//
//  Created by Alexy on 4/27/14.
//  Copyright (c) 2014 Alexy Ibrahim. All rights reserved.
//

#import "RSSItem.h"

@interface RSSItem () //<NSCoding>
-(NSArray *)imagesFromHTMLString:(NSString *)htmlstr;
@end

@implementation RSSItem

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.dictionary_Item = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(NSArray *)imagesFromItemDescription
{
    if ([self.dictionary_Item objectForKey:@"description"] || [self.dictionary_Item objectForKey:@"media:description"]) {
        return [self imagesFromHTMLString:[self.dictionary_Item objectForKey:@"description"]];
    }
    
    return nil;
}

-(NSArray *)imagesFromContent
{
    if ([self.dictionary_Item objectForKey:@"content:encoded"] || [self.dictionary_Item objectForKey:@"content"] || [self.dictionary_Item objectForKey:@"media:content"]) {
        ;
        return [self imagesFromHTMLString:(([self.dictionary_Item objectForKey:@"content:encoded"]) ? [self.dictionary_Item objectForKey:@"content:encoded"]:[self.dictionary_Item objectForKey:@"content"])];
    }
    
    return nil;
}

#pragma mark - Retrieve images from html string using regexp (private methode)

-(NSArray *)imagesFromHTMLString:(NSString *)htmlstr
{
    NSMutableArray *imagesURLStringArray = [[NSMutableArray alloc] init];
    
    NSError *error;
    
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:@"(https?)\\S*(png|jpg|jpeg|gif)"
                                  options:NSRegularExpressionCaseInsensitive
                                  error:&error];
    
    [regex enumerateMatchesInString:htmlstr
                            options:0
                              range:NSMakeRange(0, htmlstr.length)
                         usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                             [imagesURLStringArray addObject:[htmlstr substringWithRange:result.range]];
                         }];
    
    return [NSArray arrayWithArray:imagesURLStringArray];
}

//#pragma mark - NSCoding
//
//- (id)initWithCoder:(NSCoder *)aDecoder
//{
//    if (self = [super init]) {
//        _title = [aDecoder decodeObjectForKey:@"title"];
//        _itemDescription = [aDecoder decodeObjectForKey:@"itemDescription"];
//        _content = [aDecoder decodeObjectForKey:@"content"];
//        _link = [aDecoder decodeObjectForKey:@"link"];
//        _commentsLink = [aDecoder decodeObjectForKey:@"commentsLink"];
//        _commentsFeed = [aDecoder decodeObjectForKey:@"commentsFeed"];
//        _commentsCount = [aDecoder decodeObjectForKey:@"commentsCount"];
//        _pubDate = [aDecoder decodeObjectForKey:@"pubDate"];
//        _author = [aDecoder decodeObjectForKey:@"author"];
//        _guid = [aDecoder decodeObjectForKey:@"guid"];
//    }
//    return self;
//}
//
//- (void)encodeWithCoder:(NSCoder *)aCoder
//{
//    [aCoder encodeObject:self.title forKey:@"title"];
//    [aCoder encodeObject:self.itemDescription forKey:@"itemDescription"];
//    [aCoder encodeObject:self.content forKey:@"content"];
//    [aCoder encodeObject:self.link forKey:@"link"];
//    [aCoder encodeObject:self.commentsLink forKey:@"commentsLink"];
//    [aCoder encodeObject:self.commentsFeed forKey:@"commentsFeed"];
//    [aCoder encodeObject:self.commentsCount forKey:@"commentsCount"];
//    [aCoder encodeObject:self.pubDate forKey:@"pubDate"];
//    [aCoder encodeObject:self.author forKey:@"author"];
//    [aCoder encodeObject:self.guid forKey:@"guid"];
//}

#pragma mark -

- (BOOL)isEqual:(RSSItem *)object
{
    if (![object isKindOfClass:[self class]]) {
        return NO;
    }
    return [[self.dictionary_Item objectForKey:@"link"] isEqualToString:[object.dictionary_Item objectForKey:@"link"]];
}

- (NSUInteger)hash
{
    return [[self.dictionary_Item objectForKey:@"link"] hash];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: %@>", [self class], [[self.dictionary_Item objectForKey:@"title"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
}


@end
