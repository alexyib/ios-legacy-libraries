//
//  RSSItem.h
//  St. Rafka
//
//  Created by Alexy on 4/27/14.
//  Copyright (c) 2014 Alexy Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSSItem : NSObject
@property (strong, nonatomic) NSMutableDictionary *dictionary_Item; // each main tag is saved in a dictionary

-(NSArray *)imagesFromItemDescription;
-(NSArray *)imagesFromContent;
@end
