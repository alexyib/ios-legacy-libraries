//
//  Backendless+AIHelper.m
//  BackendlessPermission
//
//  Created by Alexy Ibrahim on 7/21/16.
//  Copyright © 2016 Alexy Ibrahim. All rights reserved.
//

#import "Backendless+AIHelper.h"
#import "AFNetworking.h"
#import "JSONKit.h"

#pragma mark - My Application
#define X_Backendless_Application_Id @"FF14D833-B074-2550-FF7E-76D530865300"
#define X_Backendless_REST_Secret_Key @"E51629CA-6AD6-A1AA-FFBF-AD2F72E64700"

#define domain @"https://api.backendless.com"

/*
 * Utils: Add this section before your class implementation
 */

/**
 This creates a new query parameters string from the given NSDictionary. For
 example, if the input is @{@"day":@"Tuesday", @"month":@"January"}, the output
 string will be @"day=Tuesday&month=January".
 @param queryParameters The input dictionary.
 @return The created parameters string.
 */
static NSString* NSStringFromQueryParameters(NSDictionary* queryParameters)
{
    NSMutableArray* parts = [NSMutableArray array];
    [queryParameters enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
        NSString *part = [NSString stringWithFormat: @"%@=%@",
                          [key stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding],
                          [value stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]
                          ];
        [parts addObject:part];
    }];
    return [parts componentsJoinedByString: @"&"];
}

/**
 Creates a new URL by adding the given query parameters.
 @param URL The input URL.
 @param queryParameters The query parameter dictionary to add.
 @return A new NSURL.
 */
static NSURL* NSURLByAppendingQueryParameters(NSURL* URL, NSDictionary* queryParameters)
{
    NSString* URLString = [NSString stringWithFormat:@"%@?%@",
                           [URL absoluteString],
                           NSStringFromQueryParameters(queryParameters)
                           ];
    return [NSURL URLWithString:URLString];
}

@implementation Backendless (AIHelper)

+ (NSString *)valueForKey:(NSString *)key
           fromQueryItems:(NSArray *)queryItems {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", key];
    NSURLQueryItem *queryItem = [[queryItems
                                  filteredArrayUsingPredicate:predicate]
                                 firstObject];
    return queryItem.value;
}

#pragma mark -
#pragma mark CheckApplicationPermissions
+ (void)ai_checkApplicationPermissions:(void (^)(id fetchResults, NSError *error))returnBlock {
    [self ai_fetchClass:@"Permissions" where:[NSString stringWithFormat:@"applicationName = '%@'", [[[NSBundle mainBundle] infoDictionary]  objectForKey:@"CFBundleName"]] withObjectId:nil previousFetchResults:@[] andSkip:0 sortBy:nil ascending:nil applicationId:X_Backendless_Application_Id restSecretKey:X_Backendless_REST_Secret_Key returnBlock:^(id fetchResults, NSError *error) {
        if (!error) {
            returnBlock(fetchResults, nil);
        } else {
            NSString* string_errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            NSDictionary *dictionary_FetchResult = [string_errorResponse objectFromJSONString];
            returnBlock(dictionary_FetchResult, error);
        }
    }];
}

#pragma mark - Login
#pragma mark Login
+ (void)ai_loginWith:(NSString *)login andPassword:(NSString *)password applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@/%@", domain, applicationId, restSecretKey, @"users/login"];
    
    /* Configure session, choose between:
     * defaultSessionConfiguration
     * ephemeralSessionConfiguration
     * backgroundSessionConfigurationWithIdentifier:
     And set session-wide properties, such as: HTTPAdditionalHeaders,
     HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
     */
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    /* Create session, and optionally set a NSURLSessionDelegate. */
    NSURLSession* session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
    
    /* Create the Request:
     Login (POST https://api.backendless.com/v1/users/login)
     */
    
    NSURL* URL = [NSURL URLWithString:path];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    
    // Headers
    
    [request setValue:@"REST" forHTTPHeaderField:@"application-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // JSON Body
    
    NSDictionary* parameters = @{@"login": login,
                                 @"password": password};
    
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:kNilOptions error:NULL];
    
    /* Start a new Task */
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            // Success
            id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSDictionary *dictionary_FetchResult = responseObject;
            returnBlock(dictionary_FetchResult, nil);
        }
        else {
            // Failure
            id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSDictionary *dictionary_FetchResult = responseObject;
            returnBlock(dictionary_FetchResult, error);
        }
    }];
    [task resume];
    [session finishTasksAndInvalidate];
}

#pragma mark - Fetch
#pragma mark Fetch Class
+ (void)ai_fetchClass:(NSString *)className where:(NSString *)where withObjectId:(NSString *)objectId previousFetchResults:(NSArray *)previousFetchResults andSkip:(NSUInteger)skip sortBy:(NSString *)sortBy ascending:(BOOL)ascending applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(id fetchResults, NSError *error))returnBlock {
    
    NSString *path;
    if ((![objectId isEqual:@""] &&
         ![objectId isKindOfClass:[NSNull class]] &&
         !(objectId == nil))) {
        // object is available
        path = [NSString stringWithFormat:@"%@/%@/%@/data/%@/%@", domain, applicationId, restSecretKey, className, objectId];
    } else {
        path = [NSString stringWithFormat:@"%@/%@/%@/data/%@", domain, applicationId, restSecretKey, className];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    [manager.requestSerializer setValue:@"REST" forHTTPHeaderField:@"application-type"];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithDictionary:@{@"pageSize":@"100",
                                                                                      @"offset":[NSString stringWithFormat:@"%lu", (unsigned long)skip]}];
    
    if ((![sortBy isEqual:@""] &&
         ![sortBy isKindOfClass:[NSNull class]] &&
         !(sortBy == nil))) {
        // object is available
        //        parameters[@"sortBy"] = [NSString stringWithFormat:@"%@%%20%@", sortBy, ((ascending) ? @"asc":@"desc")];
        parameters[@"sortBy"] = sortBy;
    } else {
        sortBy = @"created";
        ascending = true;
        //        parameters[@"sortBy"] = [NSString stringWithFormat:@"%@%%20%@", sortBy, ((ascending) ? @"asc":@"desc")];
        parameters[@"sortBy"] = sortBy;
    }
    
    if ((![where isEqual:@""] &&
         ![where isKindOfClass:[NSNull class]] &&
         !(where == nil))) {
        // object is available
        parameters[@"where"] = where;
    }
    
    [manager GET:path parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if ((![objectId isEqual:@""] &&
             ![objectId isKindOfClass:[NSNull class]] &&
             !(objectId == nil))) {
            // object is available
            NSDictionary *dictionary_FetchResult = responseObject;
            returnBlock(dictionary_FetchResult, nil);
        } else {
            NSMutableArray *tableRows = [previousFetchResults mutableCopy];
            NSArray *array_NewRows = responseObject;
            [tableRows addObjectsFromArray:array_NewRows];
            
            //            if ((![dictionary_FetchResult[@"nextPage"] isEqual:@""] &&
            //                 ![dictionary_FetchResult[@"nextPage"] isKindOfClass:[NSNull class]] &&
            //                 !(dictionary_FetchResult[@"nextPage"] == nil))) {
            //                // object is available
            //                NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:dictionary_FetchResult[@"nextPage"] resolvingAgainstBaseURL:NO];
            //                NSArray *queryItems = urlComponents.queryItems;
            //                NSString *offset = [self valueForKey:@"offset" fromQueryItems:queryItems];
            //
            //                [self ai_fetchClass:className where:where withObjectId:objectId previousFetchResults:[NSArray arrayWithArray:tableRows] andSkip:[offset intValue] sortBy:sortBy ascending:ascending applicationId:applicationId restSecretKey:restSecretKey returnBlock:returnBlock];
            //            } else {
            //                NSMutableDictionary *mutableDictionary_FetchResult = [dictionary_FetchResult mutableCopy];
            //                [mutableDictionary_FetchResult removeObjectForKey:@"data"];
            //                mutableDictionary_FetchResult[@"data"] = [NSArray arrayWithArray:tableRows];
            //
            //                returnBlock([NSDictionary dictionaryWithDictionary:mutableDictionary_FetchResult], nil);
            //            }
            
            returnBlock([NSArray arrayWithArray:tableRows], nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSString* string_errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSDictionary *dictionary_FetchResult = [string_errorResponse objectFromJSONString];
        returnBlock(dictionary_FetchResult, error);
    }];
}

#pragma mark Fetch Objects Count Class
+ (void)ai_fetchObjectsCountForClass:(NSString *)className where:(NSString *)where applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(id fetchResults, NSError *error))returnBlock {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@/data/%@/count", domain, applicationId, restSecretKey, className];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    /*
     NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:manager.responseSerializer.acceptableStatusCodes];
     [indexSet addIndex:500];
     manager.responseSerializer.acceptableStatusCodes = indexSet;
     */
    
    /*
     NSMutableSet *contentTypes = [[NSMutableSet alloc] initWithSet:manager.responseSerializer.acceptableContentTypes];
     [contentTypes addObject:@"text/html"];
     manager.responseSerializer.acceptableContentTypes = contentTypes;
     */
    
    // [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"X-Mashape-Key"];
    [manager.requestSerializer setValue:@"REST" forHTTPHeaderField:@"application-type"];
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    if ((![where isEqual:@""] &&
         ![where isKindOfClass:[NSNull class]] &&
         !(where == nil))) {
        // object is available
        parameters[@"where"] = where;
    }
    
    [manager GET:path parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *string_FetchResult = responseObject;
        returnBlock(string_FetchResult, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSString* string_errorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSDictionary *dictionary_FetchResult = [string_errorResponse objectFromJSONString];
        returnBlock(dictionary_FetchResult, error);
    }];
}

#pragma mark - Insert
#pragma mark Insert in Class
+ (void)ai_insertData:(NSDictionary *)data inClass:(NSString *)className applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@/data/%@", domain, applicationId, restSecretKey, className];
    
    /* Configure session, choose between:
     * defaultSessionConfiguration
     * ephemeralSessionConfiguration
     * backgroundSessionConfigurationWithIdentifier:
     And set session-wide properties, such as: HTTPAdditionalHeaders,
     HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
     */
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    /* Create session, and optionally set a NSURLSessionDelegate. */
    NSURLSession* session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
    
    /* Create the Request:
     Login (POST https://api.backendless.com/v1/users/login)
     */
    
    NSURL* URL = [NSURL URLWithString:path];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    
    // Headers
    
    [request setValue:@"REST" forHTTPHeaderField:@"application-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // JSON Body
    
    NSDictionary* parameters = [NSDictionary dictionaryWithDictionary:data];
    
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:kNilOptions error:NULL];
    
    /* Start a new Task */
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            // Success
            @try {
                // Code that can potentially throw an exception
                id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSDictionary *dictionary_FetchResult = responseObject;
                returnBlock(dictionary_FetchResult, nil);
            }
            @catch (NSException *exception) {
                // Handle an exception thrown in the @try block
                returnBlock(nil, nil);
            }
            @finally {
                // Code that gets executed whether or not an exception is thrown
            }
        }
        else {
            // Failure
            @try {
                // Code that can potentially throw an exception
                id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSDictionary *dictionary_FetchResult = responseObject;
                returnBlock(dictionary_FetchResult, error);
            }
            @catch (NSException *exception) {
                // Handle an exception thrown in the @try block
                returnBlock(nil, error);
            }
            @finally {
                // Code that gets executed whether or not an exception is thrown
            }
        }
    }];
    [task resume];
    [session finishTasksAndInvalidate];
}

#pragma mark - Update
#pragma mark Update Object
+ (void)ai_updateObjectId:(NSString *)objectId withData:(NSDictionary *)data inClass:(NSString *)className applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@/data/%@/%@", domain, applicationId, restSecretKey, className, objectId];
    
    /* Configure session, choose between:
     * defaultSessionConfiguration
     * ephemeralSessionConfiguration
     * backgroundSessionConfigurationWithIdentifier:
     And set session-wide properties, such as: HTTPAdditionalHeaders,
     HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
     */
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    /* Create session, and optionally set a NSURLSessionDelegate. */
    NSURLSession* session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
    
    /* Create the Request:
     Login (POST https://api.backendless.com/v1/users/login)
     */
    
    NSURL* URL = [NSURL URLWithString:path];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"PUT";
    
    // Headers
    
    [request setValue:@"REST" forHTTPHeaderField:@"application-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // JSON Body
    
    NSDictionary* parameters = [NSDictionary dictionaryWithDictionary:data];
    
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:parameters options:kNilOptions error:NULL];
    
    /* Start a new Task */
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            // Success
            @try {
                // Code that can potentially throw an exception
                id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSDictionary *dictionary_FetchResult = responseObject;
                returnBlock(dictionary_FetchResult, nil);
            }
            @catch (NSException *exception) {
                // Handle an exception thrown in the @try block
                returnBlock(nil, nil);
            }
            @finally {
                // Code that gets executed whether or not an exception is thrown
            }
        }
        else {
            // Failure
            @try {
                // Code that can potentially throw an exception
                id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSDictionary *dictionary_FetchResult = responseObject;
                returnBlock(dictionary_FetchResult, error);
            }
            @catch (NSException *exception) {
                // Handle an exception thrown in the @try block
                returnBlock(nil, error);
            }
            @finally {
                // Code that gets executed whether or not an exception is thrown
            }
        }
    }];
    [task resume];
    [session finishTasksAndInvalidate];
}

#pragma mark Set Relation
+ (void)ai_setRelationBetweenObjectId:(NSString *)objectId inClass:(NSString *)className andObjectIds:(NSArray *)objectIds withRelationColumnName:(NSString *)relationColumnName applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@/data/%@/%@/%@", domain, applicationId, restSecretKey, className, objectId, relationColumnName];
    
    /* Configure session, choose between:
     * defaultSessionConfiguration
     * ephemeralSessionConfiguration
     * backgroundSessionConfigurationWithIdentifier:
     And set session-wide properties, such as: HTTPAdditionalHeaders,
     HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
     */
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    /* Create session, and optionally set a NSURLSessionDelegate. */
    NSURLSession* session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
    
    /* Create the Request:
     Login (POST https://api.backendless.com/v1/users/login)
     */
    
    NSURL* URL = [NSURL URLWithString:path];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"PUT";
    
    // Headers
    
    [request setValue:@"REST" forHTTPHeaderField:@"application-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // JSON Body
    
    
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:objectIds options:kNilOptions error:NULL];
    
    /* Start a new Task */
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            // Success
            @try {
                // Code that can potentially throw an exception
                id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSDictionary *dictionary_FetchResult = responseObject;
                returnBlock(dictionary_FetchResult, nil);
            }
            @catch (NSException *exception) {
                // Handle an exception thrown in the @try block
                returnBlock(nil, nil);
            }
            @finally {
                // Code that gets executed whether or not an exception is thrown
            }
        }
        else {
            // Failure
            @try {
                // Code that can potentially throw an exception
                id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSDictionary *dictionary_FetchResult = responseObject;
                returnBlock(dictionary_FetchResult, error);
            }
            @catch (NSException *exception) {
                // Handle an exception thrown in the @try block
                returnBlock(nil, error);
            }
            @finally {
                // Code that gets executed whether or not an exception is thrown
            }
        }
    }];
    [task resume];
    [session finishTasksAndInvalidate];
}

#pragma mark - File
#pragma mark Create File
+ (void)ai_createFileWithContent:(NSString *)content fileName:(NSString *)fileName inDirectory:(NSString *)directory andOverwrite:(BOOL)overwrite applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSString *fileURL, NSError *error))returnBlock {
    
    fileName = [fileName stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@/files/binary/%@/%@", domain, applicationId, restSecretKey, directory, fileName];
    path = [path stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    /* Configure session, choose between:
     * defaultSessionConfiguration
     * ephemeralSessionConfiguration
     * backgroundSessionConfigurationWithIdentifier:
     And set session-wide properties, such as: HTTPAdditionalHeaders,
     HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
     */
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    /* Create session, and optionally set a NSURLSessionDelegate. */
    NSURLSession* session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
    
    /* Create the Request:
     Login (POST https://api.backendless.com/v1/users/login)
     */
    
    NSURL* URL = [NSURL URLWithString:path];
    NSDictionary* URLParams = @{@"overwrite": ((overwrite) ? @"true":@"false")};
    URL = NSURLByAppendingQueryParameters(URL, URLParams);
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"PUT";
    
    // Headers
    
    [request setValue:@"REST" forHTTPHeaderField:@"application-type"];
    [request setValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
    
    // JSON Body
    
    // Create NSData object
    NSData *data_Content = [content dataUsingEncoding:NSUTF8StringEncoding];
    // Get NSString from NSData object in Base64
    NSString *string_base64Encoded_Content = [data_Content base64EncodedStringWithOptions:0];
    
    request.HTTPBody = [string_base64Encoded_Content dataUsingEncoding:NSUTF8StringEncoding];
    
    /* Start a new Task */
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            // Success
            NSString* string_FetchResult = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]; // NSUTF8StringEncoding, NSASCIIStringEncoding
            NSArray *array_FetchResult_Components = [string_FetchResult componentsSeparatedByString:@"\""];
            for (int i = 0; i < [array_FetchResult_Components count]; i++) {
                if ([array_FetchResult_Components[i] hasPrefix:@"http"]) {
                    NSString *string_URL = array_FetchResult_Components[i];
                    returnBlock(string_URL, nil);
                    return;
                }
            }
            returnBlock(nil, error);
        }
        else {
            // Failure
            returnBlock(nil, error);
        }
    }];
    [task resume];
    [session finishTasksAndInvalidate];
}

#pragma mark Download File With FileName
+ (void)ai_downloadfileWithName:(NSString *)fileName fromDirectory:(NSString *)directory applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSString *fileContent, NSError *error))returnBlock {
    
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@/files/%@/%@", domain, applicationId, restSecretKey, directory, fileName];
    path = [path stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    /* Configure session, choose between:
     * defaultSessionConfiguration
     * ephemeralSessionConfiguration
     * backgroundSessionConfigurationWithIdentifier:
     And set session-wide properties, such as: HTTPAdditionalHeaders,
     HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
     */
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    /* Create session, and optionally set a NSURLSessionDelegate. */
    NSURLSession* session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
    
    /* Create the Request:
     Login (POST https://api.backendless.com/v1/users/login)
     */
    
    NSURL* URL = [NSURL URLWithString:path];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"GET";
    
    // Headers
    
    [request setValue:@"REST" forHTTPHeaderField:@"application-type"];
    [request setValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
    
    /* Start a new Task */
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            // Success
            NSString* string_FetchResult = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            returnBlock(string_FetchResult, nil);
        }
        else {
            // Failure
            returnBlock(nil, error);
        }
    }];
    [task resume];
    [session finishTasksAndInvalidate];
}

#pragma mark Download File At Path
+ (void)ai_downloadfileAtPath:(NSString *)filePath applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSString *fileContent, NSError *error))returnBlock {
    
    NSString *path = filePath;
    path = [path stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    /* Configure session, choose between:
     * defaultSessionConfiguration
     * ephemeralSessionConfiguration
     * backgroundSessionConfigurationWithIdentifier:
     And set session-wide properties, such as: HTTPAdditionalHeaders,
     HTTPCookieAcceptPolicy, requestCachePolicy or timeoutIntervalForRequest.
     */
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    /* Create session, and optionally set a NSURLSessionDelegate. */
    NSURLSession* session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
    
    /* Create the Request:
     Login (POST https://api.backendless.com/v1/users/login)
     */
    
    NSURL* URL = [NSURL URLWithString:path];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"GET";
    
    // Headers
    
    [request setValue:@"REST" forHTTPHeaderField:@"application-type"];
    [request setValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
    
    /* Start a new Task */
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            // Success
            NSString* string_FetchResult = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            returnBlock(string_FetchResult, nil);
        }
        else {
            // Failure
            returnBlock(nil, error);
        }
    }];
    [task resume];
    [session finishTasksAndInvalidate];
}

@end
