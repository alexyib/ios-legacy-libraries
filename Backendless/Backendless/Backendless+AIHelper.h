//
//  Backendless+AIHelper.h
//  BackendlessPermission
//
//  Created by Alexy Ibrahim on 7/21/16.
//  Copyright © 2016 Alexy Ibrahim. All rights reserved.
//

#import <Backendless/Backendless.h>

@interface Backendless (AIHelper)
#pragma mark -
#pragma mark Permission
+ (void)ai_checkApplicationPermissions:(void (^)(id fetchResults, NSError *error))returnBlock;

#pragma mark -
#pragma mark Login
+ (void)ai_loginWith:(NSString *)login andPassword:(NSString *)password applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

#pragma mark -
#pragma mark Fetch
+ (void)ai_fetchClass:(NSString *)className where:(NSString *)where withObjectId:(NSString *)objectId previousFetchResults:(NSArray *)previousFetchResults andSkip:(NSUInteger)skip sortBy:(NSString *)sortBy ascending:(BOOL)ascending applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(id fetchResults, NSError *error))returnBlock;
+ (void)ai_fetchObjectsCountForClass:(NSString *)className where:(NSString *)where applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(id fetchResults, NSError *error))returnBlock;

#pragma mark -
#pragma mark Insert
+ (void)ai_insertData:(NSDictionary *)data inClass:(NSString *)className applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

#pragma mark -
#pragma mark Update
+ (void)ai_updateObjectId:(NSString *)objectId withData:(NSDictionary *)data inClass:(NSString *)className applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;
+ (void)ai_setRelationBetweenObjectId:(NSString *)objectId inClass:(NSString *)className andObjectIds:(NSArray *)objectIds withRelationColumnName:(NSString *)relationColumnName applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSDictionary *fetchResults, NSError *error))returnBlock;

#pragma mark -
#pragma mark File
+ (void)ai_createFileWithContent:(NSString *)content fileName:(NSString *)fileName inDirectory:(NSString *)directory andOverwrite:(BOOL)overwrite applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSString *fileURL, NSError *error))returnBlock;
+ (void)ai_downloadfileWithName:(NSString *)fileName fromDirectory:(NSString *)directory applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSString *fileContent, NSError *error))returnBlock;
+ (void)ai_downloadfileAtPath:(NSString *)filePath applicationId:(NSString *)applicationId restSecretKey:(NSString *)restSecretKey returnBlock:(void (^)(NSString *fileContent, NSError *error))returnBlock;
@end
